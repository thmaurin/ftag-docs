"""
Small script to walk through all files in a directory and append them to files of the same name in another directory.
"""
import argparse
from pathlib import Path


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("source", help="Source directory")
    parser.add_argument("target", help="Target directory")
    parser.add_argument("mode", help="Mode to open files in", choices=["a", "w"], default="a")
    args = parser.parse_args()

    source = Path(args.source)
    target = Path(args.target)

    assert source.exists(), f"Source directory {source} does not exist."
    assert target.exists(), f"Target directory {target} does not exist."

    for path in source.rglob("*"):
        if path.is_dir():
            continue
        target_path = target / path.relative_to(source)
        if args.mode == "a" and not target_path.exists():
            raise FileNotFoundError(f"File {target_path} does not exist.")
        with target_path.open(args.mode) as target_file:
            with path.open() as source_file:
                target_file.write(source_file.read())
        print("Copied", path, "to", target_path, "in", args.mode, "mode.")


if __name__ == "__main__":
    main()
