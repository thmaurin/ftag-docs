# FTAG Calibration Recommendations

## Introduction

This page summarizes the most up-to-date release 24 FTAG status of recommendations for physics analysis.

Please note that this page includes a CDI file for `DL1dv01` that can be used for publications, and test CDI files for `GN2` that **cannot** be used for publications but are useful for R&D and analysis design and optimization.

!!! danger "Several methods are deprecated in release 24!"
    - VR Track jets are no longer supported, and will be removed
    - DL1r is no longer supported

## Recommended Tagger

1. The current recommended release 24 tagger is `DL1dv01`. See [PUB](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2020-014/) and [Plots](http://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PLOTS/FTAG-2021-004/) for comparison to `DL1r`.

2. We'll soon calibrate the transformer-based, GN2v01. See [PUB](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2022-027/)  and the [CERN Data Science Seminar](https://indico.cern.ch/event/1232499/). It is fully implemented in Athena and is available in derivations. More info on the performance of GN2 is found [here](../algs/r22-preliminary.md).

It outperforms `DL1dv01`, get excited!

### Where to get the files

All the files are accessible via `eos`, `https`, and `cvmfs`. The following base paths are equivalent:

| system | path |
| ------ | ---- |
| cvmfs | `/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency` |
| eos | `/eos/atlas/atlascerngroupdisk/asg-calib/xAODBTaggingEfficiency` |
| https | [`https://atlas-groupdata.web.cern.ch/atlas-groupdata/xAODBTaggingEfficiency/`](https://atlas-groupdata.web.cern.ch/atlas-groupdata/xAODBTaggingEfficiency) |

A full path will be of the form `<file-system>/<cdi-version>`, i.e. `/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/2023-22-13TeV-MC20-CDI-2023-09-13_v1.root`

At the moment the only supported jet collection is `AntiKt4EMPFlowJets`. We support both fixed cut and pseudo-continuous b-tagging (PCBT) working points.

The files within `xAODBTaggingEfficiency` are linked in the table below.

| Tagger | Use Case | CDI Status | Limitations | p-tag |
| ------ | -------- | ---------- | ------- | ------------ |
| DL1dv01 | Run 2 + MC20 | [done][dl1d20][^dl1d20] | No extrapolation uncertainties for PCBT | p5226 |
| DL1dv01 | Run 3 + MC23 | [done][dl1d23][^dl1d23] | Can be used for jets with pT < 400 GeV | p5226 |
| GN2v00 | Run 2&3 + MC21&23 | [Available][gn2v00] for tests[^gn2v00] | Dummy for testing GN2 | p5737 |
| GN2v01 | Run 2&3 + MC21&23 | [Available][gn2v01] for tests[^gn2v01] | Dummy for testing, requires release 25.2.2 or later | p5980 |

Notes:

- High pt extrapolation uncertainties are _only_ provided for run 2 DL1dv01, using the fixed efficiency (non-PCBT) working points.
- The GN2v00 and GN2v01 test CDIs have 100% uncertainties, but these are still not appropriate for physics analysis! We're working hard to provide calibrations, but they aren't ready yet.

[dl1d20]: https://atlas-groupdata.web.cern.ch/atlas-groupdata/xAODBTaggingEfficiency/13TeV/2023-22-13TeV-MC20-CDI-2023-09-13_v1.root
[dl1d23]: https://atlas-groupdata.web.cern.ch/atlas-groupdata/xAODBTaggingEfficiency/13p6TeV/2023-22-13TeV-MC21-CDI-2023-09-13_v1.root
[gn2v00]: https://atlas-groupdata.web.cern.ch/atlas-groupdata/xAODBTaggingEfficiency/13p6TeV/2023-22-13p6TeV-MC21-CDI_Test_2023-08-1_v1.root
[gn2v01]: https://atlas-groupdata.web.cern.ch/atlas-groupdata/xAODBTaggingEfficiency/13p6TeV/2023-02_MC23_CDI_GN2v01-noSF_bugFix.root

[^dl1d20]: File: `13TeV/2023-22-13TeV-MC20-CDI-2023-09-13_v1.root`
[^dl1d23]: File: `13p6TeV/2023-22-13TeV-MC21-CDI-2023-09-13_v1.root`
[^gn2v00]: File: `13p6TeV/2023-22-13p6TeV-MC21-CDI_Test_2023-08-1_v1.root`
[^gn2v01]: File: `13p6TeV/2023-02_MC23_CDI_GN2v01-noSF_bugFix.root `

## Technicalities for GN2v01
Five fixed cut OPx are provided: `FixedCutBEff_65`, `FixedCutBEff_70`, `FixedCutBEff_77`, `FixedCutBEff_85`, `FixedCutBEff_90`

A new output `tau` class has been added to the traditional `b`, `c` and `u` clases. For analyses, in order to use the GN2v01 tagger properly, please update the [getTaggerWeight](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency/xAODBTaggingEfficiency/BTaggingSelectionTool.h?ref_type=heads#L57) and [accept](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency/xAODBTaggingEfficiency/BTaggingSelectionTool.h?ref_type=heads#L44) to include the `ptau` score when calling from [BTaggingSelectionTool](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency/xAODBTaggingEfficiency/BTaggingSelectionTool.h?ref_type=heads). The minimum AthAnalysis (or AnalysisBase) release must be 25.2.2[^ptaufoot].

[^ptaufoot]: The `ptau` variable is technically supported in release 25.2.1 as well, but using it there is error prone, thus we recommend 25.2.2 unless you understand the risks that [were mitigated in 25.2.2][ptaubinbin].

[ptaubinbin]: https://gitlab.cern.ch/atlas/athena/-/merge_requests/69344

## Technicalities for GN2v00

We provide two sets of OPs for the analyses to test. In order to minimize the technical hurdles on the analyses, two "taggers" are implemented using the same GN2v00: GN2v00LegacyWP and GN2v00NewAliasWP.

For GN2v00LegacyWP, the traditional fixed cut OPs are provided: `FixedCutBEff_60`, `FixedCutBEff_70`, `FixedCutBEff_77` and `FixedCutBEff_85`

For `GN2v00NewAliasWP`, the operating points correspond to b-tag efficiencies of 68%, 76%, 82%, and 89%. But the names of the OPs are still the same, as summarized in the following table:

| Tagger Name In SW | Actual Tagger | OP | Real b-efficiency |
| ----------------- | ------------- | -- | ----------------- |
| GN2v00LegacyWP | GN2v00 | 85OP | 85% |
| | | 77OP | 77% |
| | | 70OP | 70% |
| | | 60OP | 60% |
| GN2v00NewAliasWP | GN2v00 | 85OP | 89% |
| | | 77OP | 82% |
| | | 70OP | 76% |
| | | 60OP | 68% |

PCBT are also defined using the above OPs. Analyses can test the corresponding PCBT by scheduling "Continuous" for either tagger.

You can config the [BTaggingSelectionTool](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency/xAODBTaggingEfficiency/BTaggingSelectionTool.h)  to use either version of the above. As a user, you only need to change the tagger name to test different OPs.  Please use releases after 24.2.16, previously the GN2 family was not implemented.


???+ info "What do I need to do if I want to test the new 82% b-efficiency OP?"
    You should implement the GN2v00NewAliasWP in your framework and apply the 77OP (FixedCutBEff_77).


## Calibration Timeline for GN2

0. Dl1dv01 is ready to be used for analyses based on MC20 and MC21. If your analysis is based on MC23 and still wants to use the Dl1dv01 CDI based on MC21, please get in touch with us.  Also, If your analysis needs an additional MC-MC map for DL1dv01, please get in touch.

1. GN2 is expected to have a significant boost in performance for both, b- vs c-jets as well as b- vs l-jets. We highly recommend analyses sensitive to FTAG to wait for this tagger and do not use Dl1dv01.  Help from physics analyses to speed-up the GN2 recommendation is very welcome, please get in touch if you would like to contribute.

???+ warning "Important: Trigger calibrations"

    No b-jet trigger calibrations will be provided for DL1dv01. We plan on only calibrating the trigger w.r.t GN2. See [BTagRel22BJetTriggerCalibration](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTagRel22BJetTriggerCalibration).

2. Full-recommendation: complete release 24 calibrations for GN2v01 is expected in the first half of 2024.

## Communication

If your analyses are planning on having publications out this year with b-tagging applied in release 24, please contact us, see [the ftag home page](../../index.md).
