# FTAG Algorithms In r22+

For r22+ analysis, two series of algorithms have been trained and released.

- The DL1d series is using the same two-tiered strategy which has been used in r21 for the late-Run-2 algorithms. Low-level taggers based on expert knowledge and specialised task provide input to a high-level feed-forward neural network. For the DL1d algorithm, the [RNNIP algorithm](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2017-003/) has been replaced by [DIPS][dips-tagger] and consequently use DIPS as input for the [DL1 algorithm][dl1-tagger].
- The [GN1][gn1-tagger] / [GN2][gn2-tagger] series employ a single architecture, directly operating on track and jet inputs. Auxiliary tasks for track origin prediction and track vertex finding improve the performance. GN1 and GN2 differ in architecture and hyperparameters, as well in size of the training dataset.

!!!tip "Doing physics? [Click here for analysis recommendations!](../../recommendations/calib/intro_for_analysts.md)"


## Recommendation as of 07.03.2024

This recommendation introduces the final version of GN2 as the sole recommended tagger for use in analyses using Run 2 and/or Run 3 data.
The tagger is named `GN2v01`, and has been trained on a high-statistic combined MC20 + MC23 dataset.

!!! info "Detailed info about the GN2v01 model is available [here][gn2v01]."

[gn2v01]: https://ftag.docs.cern.ch/algorithms/taggers/tagger_metadata/#20231205gn2v01antikt4empflow

Given the high rejection of GN2, we are making some modifications to the recommended operating points.
The traditional 70%, 77% and 85% operating points are kept, the 60% operating point has been replaced by the 65% operating point, and a new 90% operating has been added after requests from analyses.
Hence `GN2v01` has opperating points with 65%, 70%, 77%, 85% and 90% b-jet efficiency for $t\bar{t}$ jets. 
This version of GN2 also includes tau-jet scores, which are used in the discriminant calculations.
Preliminary c-tagging recommendations are also provided with `GN2v01`.
These are shown for 10%, 30% and 50% c-jet efficiency using the 77% b-tagging operating point as a veto (defining a 2D b- and c-tag WP, where jets can not be both b- and c-tagged at the same time).
The b- and c-tagging discriminants are given by: 

$$
\begin{align*}
    D_b &= \text{log}\left(\frac{p_b}{f_c \cdot p_c + f_\tau \cdot p_\tau + (1 - f_c - f_\tau) \cdot p_u}\right) \\
    D_c &= \text{log}\left(\frac{p_c}{f_b \cdot p_b + f_\tau \cdot p_\tau + (1 - f_b - f_\tau) \cdot p_u}\right),
\end{align*}
$$

where $p_b$ , $p_c$ , $p_\tau$ , and $p_u$ are the `GN2v01` probability scores for b-, c-, tau-, and light-jets repsectively. The discriminants are defined with the values $f_c = 0.2$, $f_\tau = 0.01$ for b-tagging, and $f_b = 0.3$, $f_\tau = 0.05$ for c-tagging. The preformance of the operating points is evaluated in jets from simulated $t\bar{t}$ and $Z^\prime$ events with $|\eta| < 2.5$ and $p_{T}$ cuts given in the plots. The samples used in this section are:

| Sample | H5 ntuples |
| ------ | ------------------------ |
| $t\bar{t}$ mc20 | user.nekumari.410470.e6337_s3681_r13144_p6057.tdd.EMPFlow_kfold.24_2_37.24-02-15_Dump_run3_feb24_output.h5 |
| $t\bar{t}$ mc23 | user.nekumari.601589.e8549_s4162_r14622_p6057.tdd.EMPFlow_kfold.24_2_37.24-02-15_Dump_run3_feb24_output.h5 |
|$Z^\prime$ mc20 | user.nekumari.800030.e7954_s3681_r13144_p6057.tdd.EMPFlow_kfold.24_2_37.24-02-15_Dump_run3_feb24_output.h5 |
|$Z^\prime$ mc23 | user.nekumari.800030.e8514_s4162_r14622_p6057.tdd.EMPFlow_kfold.24_2_37.24-02-15_Dump_run3_feb24_output.h5 |

Discriminant cuts for the operating points are calculated with the $t\bar{t}$ mc20 and mc23 samples where the jets are mixed in a 1:1 ratio, hence the signal efficiency will vary depending on the sample, and is shown in the tables below. Most importantly, this means that the signal efficiency of the operating points are much lower for the $Z^\prime$ samples as separation of signal and background is harder at very high $p_\text{T}$. All results are obtained from 10 million jets from each sample.

### GN2v01 - b-tagging
`GN2v01` Discriminant defined as $D_b = \text{log}\left(\frac{p_b}{f_c \cdot p_c + f_\tau \cdot p_\tau + (1 - f_c - f_\tau) \cdot p_u}\right)$ with $f_c = 0.2$ and $f_\tau = 0.01$.  

=== "$t\bar{t}$ mc20"

	| Name   | Efficiency operating point | Discriminant cut | b-efficiency | c-efficiency | light-flavour efficiency | tau-efficiency | c-rejection | light-flavour rejection | tau-rejection |
	| ------ | ------------------------ | ---------------- | ------------ | ------------ | ------------------------ | ----------- | ----------------------- | ----------------------- | ----------------------- |
	| GN2v01 | FixedCutBEff_65 | 2.658 | 0.64 | 0.00900 | 0.00024 | 0.00131 | 111.05 | 4174.06 | 760.52 |
	| GN2v01 | FixedCutBEff_70 | 1.877 | 0.69 | 0.02012 | 0.00059 | 0.00329 | 49.70 | 1695.80 | 304.21 |
	| GN2v01 | FixedCutBEff_77 | 0.828 | 0.76 | 0.05794 | 0.00227 | 0.01437 | 17.26 | 439.91 | 69.61 |
	| GN2v01 | FixedCutBEff_85 | -0.396 | 0.84 | 0.17261 | 0.01108 | 0.07952 | 5.79 | 90.23 | 12.58 |
	| GN2v01 | FixedCutBEff_90 | -1.351 | 0.90 | 0.32655 | 0.03450 | 0.19172 | 3.06 | 28.99 | 5.22 |

	![ROC curves for light-jet, c-jet and tau-jet rejection as a function of b-tagging efficiency](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_roc_roc_DL1dv01_b_mc20.png)

	??? example "GN2v01WP: FixedCutBEff_65 "

		- INTERNAL b-jet efficiency as a function of jet pT for the FixedCutBEff_65 WP

		![b-jet efficiency as a function of jet pT for the FixedCutBEff_65 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_bjets_eff_vs_pt_wp_0p642_profile_fixed_cut_FixedCutBEff_65_mc20_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutBEff_65 WP

		![light-jet rejection as a function of jet pT for the FixedCutBEff_65 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_ujets_rej_vs_pt_wp_0p642_profile_fixed_cut_FixedCutBEff_65_mc20_.png)

		- INTERNAL c-jet rejection as a function of jet pT for the FixedCutBEff_65 WP

		![c-jet rejection as a function of jet pT for the FixedCutBEff_65 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_cjets_rej_vs_pt_wp_0p642_profile_fixed_cut_FixedCutBEff_65_mc20_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutBEff_65 WP

		![tau-jet rejection as a function of jet pT for the FixedCutBEff_65 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_taujets_rej_vs_pt_wp_0p642_profile_fixed_cut_FixedCutBEff_65_mc20_.png)

	??? example "GN2v01WP: FixedCutBEff_70 "

		- INTERNAL b-jet efficiency as a function of jet pT for the FixedCutBEff_70 WP

		![b-jet efficiency as a function of jet pT for the FixedCutBEff_70 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_bjets_eff_vs_pt_wp_0p692_profile_fixed_cut_FixedCutBEff_70_mc20_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutBEff_70 WP

		![light-jet rejection as a function of jet pT for the FixedCutBEff_70 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_ujets_rej_vs_pt_wp_0p692_profile_fixed_cut_FixedCutBEff_70_mc20_.png)

		- INTERNAL c-jet rejection as a function of jet pT for the FixedCutBEff_70 WP

		![c-jet rejection as a function of jet pT for the FixedCutBEff_70 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_cjets_rej_vs_pt_wp_0p692_profile_fixed_cut_FixedCutBEff_70_mc20_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutBEff_70 WP

		![tau-jet rejection as a function of jet pT for the FixedCutBEff_70 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_taujets_rej_vs_pt_wp_0p692_profile_fixed_cut_FixedCutBEff_70_mc20_.png)

	??? example "GN2v01WP: FixedCutBEff_77 "

		- INTERNAL b-jet efficiency as a function of jet pT for the FixedCutBEff_77 WP

		![b-jet efficiency as a function of jet pT for the FixedCutBEff_77 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_bjets_eff_vs_pt_wp_0p763_profile_fixed_cut_FixedCutBEff_77_mc20_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutBEff_77 WP

		![light-jet rejection as a function of jet pT for the FixedCutBEff_77 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_ujets_rej_vs_pt_wp_0p763_profile_fixed_cut_FixedCutBEff_77_mc20_.png)

		- INTERNAL c-jet rejection as a function of jet pT for the FixedCutBEff_77 WP

		![c-jet rejection as a function of jet pT for the FixedCutBEff_77 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_cjets_rej_vs_pt_wp_0p763_profile_fixed_cut_FixedCutBEff_77_mc20_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutBEff_77 WP

		![tau-jet rejection as a function of jet pT for the FixedCutBEff_77 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_taujets_rej_vs_pt_wp_0p763_profile_fixed_cut_FixedCutBEff_77_mc20_.png)

	??? example "GN2v01WP: FixedCutBEff_85 "

		- INTERNAL b-jet efficiency as a function of jet pT for the FixedCutBEff_85 WP

		![b-jet efficiency as a function of jet pT for the FixedCutBEff_85 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_bjets_eff_vs_pt_wp_0p844_profile_fixed_cut_FixedCutBEff_85_mc20_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutBEff_85 WP

		![light-jet rejection as a function of jet pT for the FixedCutBEff_85 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_ujets_rej_vs_pt_wp_0p844_profile_fixed_cut_FixedCutBEff_85_mc20_.png)

		- INTERNAL c-jet rejection as a function of jet pT for the FixedCutBEff_85 WP

		![c-jet rejection as a function of jet pT for the FixedCutBEff_85 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_cjets_rej_vs_pt_wp_0p844_profile_fixed_cut_FixedCutBEff_85_mc20_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutBEff_85 WP

		![tau-jet rejection as a function of jet pT for the FixedCutBEff_85 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_taujets_rej_vs_pt_wp_0p844_profile_fixed_cut_FixedCutBEff_85_mc20_.png)

	??? example "GN2v01WP: FixedCutBEff_90 "

		- INTERNAL b-jet efficiency as a function of jet pT for the FixedCutBEff_90 WP

		![b-jet efficiency as a function of jet pT for the FixedCutBEff_90 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_bjets_eff_vs_pt_wp_0p896_profile_fixed_cut_FixedCutBEff_90_mc20_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutBEff_90 WP

		![light-jet rejection as a function of jet pT for the FixedCutBEff_90 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_ujets_rej_vs_pt_wp_0p896_profile_fixed_cut_FixedCutBEff_90_mc20_.png)

		- INTERNAL c-jet rejection as a function of jet pT for the FixedCutBEff_90 WP

		![c-jet rejection as a function of jet pT for the FixedCutBEff_90 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_cjets_rej_vs_pt_wp_0p896_profile_fixed_cut_FixedCutBEff_90_mc20_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutBEff_90 WP

		![tau-jet rejection as a function of jet pT for the FixedCutBEff_90 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_taujets_rej_vs_pt_wp_0p896_profile_fixed_cut_FixedCutBEff_90_mc20_.png)

=== "$t\bar{t}$ mc23"

	| Name   | Efficiency operating point | Discriminant cut | b-efficiency | c-efficiency | light-flavour efficiency | tau-efficiency | c-rejection | light-flavour rejection | tau-rejection |
	| ------ | ------------------------ | ---------------- | ------------ | ------------ | ------------------------ | ----------- | ----------------------- | ----------------------- | ----------------------- |
	| GN2v01 | FixedCutBEff_65 | 2.658 | 0.66 | 0.00955 | 0.00026 | 0.00135 | 104.68 | 3812.71 | 738.86 |
	| GN2v01 | FixedCutBEff_70 | 1.877 | 0.71 | 0.02112 | 0.00061 | 0.00333 | 47.35 | 1636.39 | 300.75 |
	| GN2v01 | FixedCutBEff_77 | 0.828 | 0.78 | 0.05959 | 0.00222 | 0.01371 | 16.78 | 450.25 | 72.95 |
	| GN2v01 | FixedCutBEff_85 | -0.396 | 0.85 | 0.17752 | 0.01061 | 0.07623 | 5.63 | 94.26 | 13.12 |
	| GN2v01 | FixedCutBEff_90 | -1.351 | 0.90 | 0.33389 | 0.03278 | 0.18857 | 2.99 | 30.51 | 5.30 |

	![ROC curves for light-jet, c-jet and tau-jet rejection as a function of b-tagging efficiency](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_roc_roc_DL1dv01_b_mc23.png)

	??? example "GN2v01WP: FixedCutBEff_65 "

		- INTERNAL b-jet efficiency as a function of jet pT for the FixedCutBEff_65 WP

		![b-jet efficiency as a function of jet pT for the FixedCutBEff_65 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_bjets_eff_vs_pt_wp_0p658_profile_fixed_cut_FixedCutBEff_65_mc23_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutBEff_65 WP

		![light-jet rejection as a function of jet pT for the FixedCutBEff_65 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_ujets_rej_vs_pt_wp_0p658_profile_fixed_cut_FixedCutBEff_65_mc23_.png)

		- INTERNAL c-jet rejection as a function of jet pT for the FixedCutBEff_65 WP

		![c-jet rejection as a function of jet pT for the FixedCutBEff_65 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_cjets_rej_vs_pt_wp_0p658_profile_fixed_cut_FixedCutBEff_65_mc23_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutBEff_65 WP

		![tau-jet rejection as a function of jet pT for the FixedCutBEff_65 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_taujets_rej_vs_pt_wp_0p658_profile_fixed_cut_FixedCutBEff_65_mc23_.png)

	??? example "GN2v01WP: FixedCutBEff_70 "

		- INTERNAL b-jet efficiency as a function of jet pT for the FixedCutBEff_70 WP

		![b-jet efficiency as a function of jet pT for the FixedCutBEff_70 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_bjets_eff_vs_pt_wp_0p707_profile_fixed_cut_FixedCutBEff_70_mc23_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutBEff_70 WP

		![light-jet rejection as a function of jet pT for the FixedCutBEff_70 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_ujets_rej_vs_pt_wp_0p707_profile_fixed_cut_FixedCutBEff_70_mc23_.png)

		- INTERNAL c-jet rejection as a function of jet pT for the FixedCutBEff_70 WP

		![c-jet rejection as a function of jet pT for the FixedCutBEff_70 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_cjets_rej_vs_pt_wp_0p707_profile_fixed_cut_FixedCutBEff_70_mc23_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutBEff_70 WP

		![tau-jet rejection as a function of jet pT for the FixedCutBEff_70 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_taujets_rej_vs_pt_wp_0p707_profile_fixed_cut_FixedCutBEff_70_mc23_.png)

	??? example "GN2v01WP: FixedCutBEff_77 "

		- INTERNAL b-jet efficiency as a function of jet pT for the FixedCutBEff_77 WP

		![b-jet efficiency as a function of jet pT for the FixedCutBEff_77 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_bjets_eff_vs_pt_wp_0p777_profile_fixed_cut_FixedCutBEff_77_mc23_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutBEff_77 WP

		![light-jet rejection as a function of jet pT for the FixedCutBEff_77 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_ujets_rej_vs_pt_wp_0p777_profile_fixed_cut_FixedCutBEff_77_mc23_.png)

		- INTERNAL c-jet rejection as a function of jet pT for the FixedCutBEff_77 WP

		![c-jet rejection as a function of jet pT for the FixedCutBEff_77 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_cjets_rej_vs_pt_wp_0p777_profile_fixed_cut_FixedCutBEff_77_mc23_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutBEff_77 WP

		![tau-jet rejection as a function of jet pT for the FixedCutBEff_77 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_taujets_rej_vs_pt_wp_0p777_profile_fixed_cut_FixedCutBEff_77_mc23_.png)

	??? example "GN2v01WP: FixedCutBEff_85 "

		- INTERNAL b-jet efficiency as a function of jet pT for the FixedCutBEff_85 WP

		![b-jet efficiency as a function of jet pT for the FixedCutBEff_85 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_bjets_eff_vs_pt_wp_0p855_profile_fixed_cut_FixedCutBEff_85_mc23_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutBEff_85 WP

		![light-jet rejection as a function of jet pT for the FixedCutBEff_85 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_ujets_rej_vs_pt_wp_0p855_profile_fixed_cut_FixedCutBEff_85_mc23_.png)

		- INTERNAL c-jet rejection as a function of jet pT for the FixedCutBEff_85 WP

		![c-jet rejection as a function of jet pT for the FixedCutBEff_85 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_cjets_rej_vs_pt_wp_0p855_profile_fixed_cut_FixedCutBEff_85_mc23_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutBEff_85 WP

		![tau-jet rejection as a function of jet pT for the FixedCutBEff_85 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_taujets_rej_vs_pt_wp_0p855_profile_fixed_cut_FixedCutBEff_85_mc23_.png)

	??? example "GN2v01WP: FixedCutBEff_90 "

		- INTERNAL b-jet efficiency as a function of jet pT for the FixedCutBEff_90 WP

		![b-jet efficiency as a function of jet pT for the FixedCutBEff_90 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_bjets_eff_vs_pt_wp_0p904_profile_fixed_cut_FixedCutBEff_90_mc23_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutBEff_90 WP

		![light-jet rejection as a function of jet pT for the FixedCutBEff_90 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_ujets_rej_vs_pt_wp_0p904_profile_fixed_cut_FixedCutBEff_90_mc23_.png)

		- INTERNAL c-jet rejection as a function of jet pT for the FixedCutBEff_90 WP

		![c-jet rejection as a function of jet pT for the FixedCutBEff_90 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_cjets_rej_vs_pt_wp_0p904_profile_fixed_cut_FixedCutBEff_90_mc23_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutBEff_90 WP

		![tau-jet rejection as a function of jet pT for the FixedCutBEff_90 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_bjets_taujets_rej_vs_pt_wp_0p904_profile_fixed_cut_FixedCutBEff_90_mc23_.png)

=== "$Z^\prime$ mc20"

	| Name   | Efficiency operating point | Discriminant cut | b-efficiency | c-efficiency | light-flavour efficiency | tau-efficiency | c-rejection | light-flavour rejection | tau-rejection |
	| ------ | ------------------------ | ---------------- | ------------ | ------------ | ------------------------ | ----------- | ----------------------- | ----------------------- | ----------------------- |
	| GN2v01 | FixedCutBEff_65 | 2.658 | 0.23 | 0.00561 | 0.00046 | 0.00044 | 178.14 | 2177.36 | 2297.52 |
	| GN2v01 | FixedCutBEff_70 | 1.877 | 0.29 | 0.01455 | 0.00127 | 0.00085 | 68.75 | 785.25 | 1183.05 |
	| GN2v01 | FixedCutBEff_77 | 0.828 | 0.41 | 0.05027 | 0.00570 | 0.00224 | 19.89 | 175.35 | 446.56 |
	| GN2v01 | FixedCutBEff_85 | -0.396 | 0.63 | 0.19200 | 0.03993 | 0.00888 | 5.21 | 25.04 | 112.67 |
	| GN2v01 | FixedCutBEff_90 | -1.351 | 0.81 | 0.41110 | 0.12547 | 0.02231 | 2.43 | 7.97 | 44.82 |

	![ROC curves for light-jet, c-jet and tau-jet rejection as a function of b-tagging efficiency](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_roc_roc_DL1dv01_b_mc20.png)

	??? example "GN2v01WP: FixedCutBEff_65 "

		- INTERNAL b-jet efficiency as a function of jet pT for the FixedCutBEff_65 WP

		![b-jet efficiency as a function of jet pT for the FixedCutBEff_65 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_bjets_eff_vs_pt_wp_0p229_profile_fixed_cut_FixedCutBEff_65_mc20_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutBEff_65 WP

		![light-jet rejection as a function of jet pT for the FixedCutBEff_65 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_ujets_rej_vs_pt_wp_0p229_profile_fixed_cut_FixedCutBEff_65_mc20_.png)

		- INTERNAL c-jet rejection as a function of jet pT for the FixedCutBEff_65 WP

		![c-jet rejection as a function of jet pT for the FixedCutBEff_65 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_cjets_rej_vs_pt_wp_0p229_profile_fixed_cut_FixedCutBEff_65_mc20_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutBEff_65 WP

		![tau-jet rejection as a function of jet pT for the FixedCutBEff_65 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_taujets_rej_vs_pt_wp_0p229_profile_fixed_cut_FixedCutBEff_65_mc20_.png)

	??? example "GN2v01WP: FixedCutBEff_70 "

		- INTERNAL b-jet efficiency as a function of jet pT for the FixedCutBEff_70 WP

		![b-jet efficiency as a function of jet pT for the FixedCutBEff_70 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_bjets_eff_vs_pt_wp_0p293_profile_fixed_cut_FixedCutBEff_70_mc20_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutBEff_70 WP

		![light-jet rejection as a function of jet pT for the FixedCutBEff_70 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_ujets_rej_vs_pt_wp_0p293_profile_fixed_cut_FixedCutBEff_70_mc20_.png)

		- INTERNAL c-jet rejection as a function of jet pT for the FixedCutBEff_70 WP

		![c-jet rejection as a function of jet pT for the FixedCutBEff_70 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_cjets_rej_vs_pt_wp_0p293_profile_fixed_cut_FixedCutBEff_70_mc20_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutBEff_70 WP

		![tau-jet rejection as a function of jet pT for the FixedCutBEff_70 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_taujets_rej_vs_pt_wp_0p293_profile_fixed_cut_FixedCutBEff_70_mc20_.png)

	??? example "GN2v01WP: FixedCutBEff_77 "

		- INTERNAL b-jet efficiency as a function of jet pT for the FixedCutBEff_77 WP

		![b-jet efficiency as a function of jet pT for the FixedCutBEff_77 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_bjets_eff_vs_pt_wp_0p41_profile_fixed_cut_FixedCutBEff_77_mc20_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutBEff_77 WP

		![light-jet rejection as a function of jet pT for the FixedCutBEff_77 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_ujets_rej_vs_pt_wp_0p41_profile_fixed_cut_FixedCutBEff_77_mc20_.png)

		- INTERNAL c-jet rejection as a function of jet pT for the FixedCutBEff_77 WP

		![c-jet rejection as a function of jet pT for the FixedCutBEff_77 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_cjets_rej_vs_pt_wp_0p41_profile_fixed_cut_FixedCutBEff_77_mc20_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutBEff_77 WP

		![tau-jet rejection as a function of jet pT for the FixedCutBEff_77 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_taujets_rej_vs_pt_wp_0p41_profile_fixed_cut_FixedCutBEff_77_mc20_.png)

	??? example "GN2v01WP: FixedCutBEff_85 "

		- INTERNAL b-jet efficiency as a function of jet pT for the FixedCutBEff_85 WP

		![b-jet efficiency as a function of jet pT for the FixedCutBEff_85 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_bjets_eff_vs_pt_wp_0p635_profile_fixed_cut_FixedCutBEff_85_mc20_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutBEff_85 WP

		![light-jet rejection as a function of jet pT for the FixedCutBEff_85 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_ujets_rej_vs_pt_wp_0p635_profile_fixed_cut_FixedCutBEff_85_mc20_.png)

		- INTERNAL c-jet rejection as a function of jet pT for the FixedCutBEff_85 WP

		![c-jet rejection as a function of jet pT for the FixedCutBEff_85 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_cjets_rej_vs_pt_wp_0p635_profile_fixed_cut_FixedCutBEff_85_mc20_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutBEff_85 WP

		![tau-jet rejection as a function of jet pT for the FixedCutBEff_85 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_taujets_rej_vs_pt_wp_0p635_profile_fixed_cut_FixedCutBEff_85_mc20_.png)

	??? example "GN2v01WP: FixedCutBEff_90 "

		- INTERNAL b-jet efficiency as a function of jet pT for the FixedCutBEff_90 WP

		![b-jet efficiency as a function of jet pT for the FixedCutBEff_90 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_bjets_eff_vs_pt_wp_0p81_profile_fixed_cut_FixedCutBEff_90_mc20_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutBEff_90 WP

		![light-jet rejection as a function of jet pT for the FixedCutBEff_90 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_ujets_rej_vs_pt_wp_0p81_profile_fixed_cut_FixedCutBEff_90_mc20_.png)

		- INTERNAL c-jet rejection as a function of jet pT for the FixedCutBEff_90 WP

		![c-jet rejection as a function of jet pT for the FixedCutBEff_90 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_cjets_rej_vs_pt_wp_0p81_profile_fixed_cut_FixedCutBEff_90_mc20_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutBEff_90 WP

		![tau-jet rejection as a function of jet pT for the FixedCutBEff_90 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_taujets_rej_vs_pt_wp_0p81_profile_fixed_cut_FixedCutBEff_90_mc20_.png)

=== "$Z^\prime$ mc23"

	| Name   | Efficiency operating point | Discriminant cut | b-efficiency | c-efficiency | light-flavour efficiency | tau-efficiency | c-rejection | light-flavour rejection | tau-rejection |
	| ------ | ------------------------ | ---------------- | ------------ | ------------ | ------------------------ | ----------- | ----------------------- | ----------------------- | ----------------------- |
	| GN2v01 | FixedCutBEff_65 | 2.658 | 0.23 | 0.00795 | 0.00055 | 0.00040 | 125.73 | 1825.17 | 2474.17 |
	| GN2v01 | FixedCutBEff_70 | 1.877 | 0.29 | 0.02028 | 0.00154 | 0.00086 | 49.31 | 648.52 | 1156.99 |
	| GN2v01 | FixedCutBEff_77 | 0.828 | 0.41 | 0.06720 | 0.00695 | 0.00237 | 14.88 | 143.83 | 422.10 |
	| GN2v01 | FixedCutBEff_85 | -0.396 | 0.63 | 0.22581 | 0.04631 | 0.00921 | 4.43 | 21.59 | 108.59 |
	| GN2v01 | FixedCutBEff_90 | -1.351 | 0.81 | 0.44922 | 0.14100 | 0.02325 | 2.23 | 7.09 | 43.01 |

	![ROC curves for light-jet, c-jet and tau-jet rejection as a function of b-tagging efficiency](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_roc_roc_DL1dv01_b_mc23.png)

	??? example "GN2v01WP: FixedCutBEff_65 "

		- INTERNAL b-jet efficiency as a function of jet pT for the FixedCutBEff_65 WP

		![b-jet efficiency as a function of jet pT for the FixedCutBEff_65 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_bjets_eff_vs_pt_wp_0p225_profile_fixed_cut_FixedCutBEff_65_mc23_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutBEff_65 WP

		![light-jet rejection as a function of jet pT for the FixedCutBEff_65 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_ujets_rej_vs_pt_wp_0p225_profile_fixed_cut_FixedCutBEff_65_mc23_.png)

		- INTERNAL c-jet rejection as a function of jet pT for the FixedCutBEff_65 WP

		![c-jet rejection as a function of jet pT for the FixedCutBEff_65 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_cjets_rej_vs_pt_wp_0p225_profile_fixed_cut_FixedCutBEff_65_mc23_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutBEff_65 WP

		![tau-jet rejection as a function of jet pT for the FixedCutBEff_65 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_taujets_rej_vs_pt_wp_0p225_profile_fixed_cut_FixedCutBEff_65_mc23_.png)

	??? example "GN2v01WP: FixedCutBEff_70 "

		- INTERNAL b-jet efficiency as a function of jet pT for the FixedCutBEff_70 WP

		![b-jet efficiency as a function of jet pT for the FixedCutBEff_70 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_bjets_eff_vs_pt_wp_0p289_profile_fixed_cut_FixedCutBEff_70_mc23_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutBEff_70 WP

		![light-jet rejection as a function of jet pT for the FixedCutBEff_70 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_ujets_rej_vs_pt_wp_0p289_profile_fixed_cut_FixedCutBEff_70_mc23_.png)

		- INTERNAL c-jet rejection as a function of jet pT for the FixedCutBEff_70 WP

		![c-jet rejection as a function of jet pT for the FixedCutBEff_70 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_cjets_rej_vs_pt_wp_0p289_profile_fixed_cut_FixedCutBEff_70_mc23_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutBEff_70 WP

		![tau-jet rejection as a function of jet pT for the FixedCutBEff_70 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_taujets_rej_vs_pt_wp_0p289_profile_fixed_cut_FixedCutBEff_70_mc23_.png)

	??? example "GN2v01WP: FixedCutBEff_77 "

		- INTERNAL b-jet efficiency as a function of jet pT for the FixedCutBEff_77 WP

		![b-jet efficiency as a function of jet pT for the FixedCutBEff_77 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_bjets_eff_vs_pt_wp_0p407_profile_fixed_cut_FixedCutBEff_77_mc23_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutBEff_77 WP

		![light-jet rejection as a function of jet pT for the FixedCutBEff_77 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_ujets_rej_vs_pt_wp_0p407_profile_fixed_cut_FixedCutBEff_77_mc23_.png)

		- INTERNAL c-jet rejection as a function of jet pT for the FixedCutBEff_77 WP

		![c-jet rejection as a function of jet pT for the FixedCutBEff_77 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_cjets_rej_vs_pt_wp_0p407_profile_fixed_cut_FixedCutBEff_77_mc23_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutBEff_77 WP

		![tau-jet rejection as a function of jet pT for the FixedCutBEff_77 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_taujets_rej_vs_pt_wp_0p407_profile_fixed_cut_FixedCutBEff_77_mc23_.png)

	??? example "GN2v01WP: FixedCutBEff_85 "

		- INTERNAL b-jet efficiency as a function of jet pT for the FixedCutBEff_85 WP

		![b-jet efficiency as a function of jet pT for the FixedCutBEff_85 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_bjets_eff_vs_pt_wp_0p634_profile_fixed_cut_FixedCutBEff_85_mc23_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutBEff_85 WP

		![light-jet rejection as a function of jet pT for the FixedCutBEff_85 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_ujets_rej_vs_pt_wp_0p634_profile_fixed_cut_FixedCutBEff_85_mc23_.png)

		- INTERNAL c-jet rejection as a function of jet pT for the FixedCutBEff_85 WP

		![c-jet rejection as a function of jet pT for the FixedCutBEff_85 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_cjets_rej_vs_pt_wp_0p634_profile_fixed_cut_FixedCutBEff_85_mc23_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutBEff_85 WP

		![tau-jet rejection as a function of jet pT for the FixedCutBEff_85 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_taujets_rej_vs_pt_wp_0p634_profile_fixed_cut_FixedCutBEff_85_mc23_.png)

	??? example "GN2v01WP: FixedCutBEff_90 "

		- INTERNAL b-jet efficiency as a function of jet pT for the FixedCutBEff_90 WP

		![b-jet efficiency as a function of jet pT for the FixedCutBEff_90 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_bjets_eff_vs_pt_wp_0p812_profile_fixed_cut_FixedCutBEff_90_mc23_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutBEff_90 WP

		![light-jet rejection as a function of jet pT for the FixedCutBEff_90 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_ujets_rej_vs_pt_wp_0p812_profile_fixed_cut_FixedCutBEff_90_mc23_.png)

		- INTERNAL c-jet rejection as a function of jet pT for the FixedCutBEff_90 WP

		![c-jet rejection as a function of jet pT for the FixedCutBEff_90 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_cjets_rej_vs_pt_wp_0p812_profile_fixed_cut_FixedCutBEff_90_mc23_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutBEff_90 WP

		![tau-jet rejection as a function of jet pT for the FixedCutBEff_90 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_bjets_taujets_rej_vs_pt_wp_0p812_profile_fixed_cut_FixedCutBEff_90_mc23_.png)
 

### GN2v01 - c-tagging

For the c-tagging working points, a 77% WP b-tagging veto ($D_b > 0.828$) is used. The b-, c-, light- and tau-efficiencies for this b-veto is shown together with the efficiencies and rejections of the c-tagging working points themselves. In the ROC and $p_\text{T}$ performance plots no b-veto is used for easier comparison between the taggers and to show the performance of standalone c-tagging.

`GN2v01` Discriminant defined as $D_c = \text{log}\left(\frac{p_c}{f_b \cdot p_b + f_\tau \cdot p_\tau + (1 - f_b - f_\tau) \cdot p_u}\right)$ with $f_b = 0.3$ and $f_\tau = 0.05$.

=== "$t\bar{t}$ mc20"

	| Name   | Efficiency operating point | Discriminant cut | c-efficiency | b-efficiency | light-flavour efficiency | tau-efficiency | b-rejection | light-flavour rejection | tau-rejection |
	| ------ | ------------------------ | ---------------- | ------------ | ------------ | ------------------------ | ----------- | ----------------------- | ----------------------- | ----------------------- |
	| GN2v01 | FixedCutCEff_10 | 3.958 | 0.090 | 0.00536 | 0.00007 | 0.00062 | 186.56 | 14020.57 | 1614.85  |
	| GN2v01 | FixedCutCEff_30 | 2.09 | 0.275 | 0.05321 | 0.00279 | 0.01484 | 18.79 | 358.48 | 67.37  |
	| GN2v01 | FixedCutCEff_50 | 0.503 | 0.466 | 0.12239 | 0.03733 | 0.10485 | 8.17 | 26.79 | 9.54  |
	| GN2v01 | B disc cut | 0.828 | 0.05794 | 0.76298 | 0.00227 | 0.01437 | - | - | - |

	![ROC curves for light-jet, b-jet and tau-jet rejection as a function of c-tagging efficiency](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_roc_roc_DL1dv01_c_mc20.png)

	??? example "GN2v01WP: FixedCutCEff_10 "

		- INTERNAL c-jet efficiency as a function of jet pT for the FixedCutCEff_10 WP

		![c-jet efficiency as a function of jet pT for the FixedCutCEff_10 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_cjets_eff_vs_pt_wp_0p09_profile_fixed_cut_FixedCutCEff_10_mc20_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutCEff_10 WP

		![light-jet rejection as a function of jet pT for the FixedCutCEff_10 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_ujets_rej_vs_pt_wp_0p09_profile_fixed_cut_FixedCutCEff_10_mc20_.png)

		- INTERNAL b-jet rejection as a function of jet pT for the FixedCutCEff_10 WP

		![b-jet rejection as a function of jet pT for the FixedCutCEff_10 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_bjets_rej_vs_pt_wp_0p09_profile_fixed_cut_FixedCutCEff_10_mc20_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutCEff_10 WP

		![tau-jet rejection as a function of jet pT for the FixedCutCEff_10 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_taujets_rej_vs_pt_wp_0p09_profile_fixed_cut_FixedCutCEff_10_mc20_.png)

	??? example "GN2v01WP: FixedCutCEff_30 "

		- INTERNAL c-jet efficiency as a function of jet pT for the FixedCutCEff_30 WP

		![c-jet efficiency as a function of jet pT for the FixedCutCEff_30 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_cjets_eff_vs_pt_wp_0p275_profile_fixed_cut_FixedCutCEff_30_mc20_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutCEff_30 WP

		![light-jet rejection as a function of jet pT for the FixedCutCEff_30 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_ujets_rej_vs_pt_wp_0p275_profile_fixed_cut_FixedCutCEff_30_mc20_.png)

		- INTERNAL b-jet rejection as a function of jet pT for the FixedCutCEff_30 WP

		![b-jet rejection as a function of jet pT for the FixedCutCEff_30 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_bjets_rej_vs_pt_wp_0p275_profile_fixed_cut_FixedCutCEff_30_mc20_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutCEff_30 WP

		![tau-jet rejection as a function of jet pT for the FixedCutCEff_30 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_taujets_rej_vs_pt_wp_0p275_profile_fixed_cut_FixedCutCEff_30_mc20_.png)

	??? example "GN2v01WP: FixedCutCEff_50 "

		- INTERNAL c-jet efficiency as a function of jet pT for the FixedCutCEff_50 WP

		![c-jet efficiency as a function of jet pT for the FixedCutCEff_50 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_cjets_eff_vs_pt_wp_0p466_profile_fixed_cut_FixedCutCEff_50_mc20_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutCEff_50 WP

		![light-jet rejection as a function of jet pT for the FixedCutCEff_50 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_ujets_rej_vs_pt_wp_0p466_profile_fixed_cut_FixedCutCEff_50_mc20_.png)

		- INTERNAL b-jet rejection as a function of jet pT for the FixedCutCEff_50 WP

		![b-jet rejection as a function of jet pT for the FixedCutCEff_50 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_bjets_rej_vs_pt_wp_0p466_profile_fixed_cut_FixedCutCEff_50_mc20_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutCEff_50 WP

		![tau-jet rejection as a function of jet pT for the FixedCutCEff_50 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_taujets_rej_vs_pt_wp_0p466_profile_fixed_cut_FixedCutCEff_50_mc20_.png)

=== "$t\bar{t}$ mc23"

	| Name   | Efficiency operating point | Discriminant cut | c-efficiency | b-efficiency | light-flavour efficiency | tau-efficiency | b-rejection | light-flavour rejection | tau-rejection |
	| ------ | ------------------------ | ---------------- | ------------ | ------------ | ------------------------ | ----------- | ----------------------- | ----------------------- | ----------------------- |
	| GN2v01 | FixedCutCEff_10 | 3.958 | 0.097 | 0.00527 | 0.00007 | 0.00058 | 189.80 | 14167.81 | 1732.13  |
	| GN2v01 | FixedCutCEff_30 | 2.09 | 0.288 | 0.05176 | 0.00254 | 0.01381 | 19.32 | 393.20 | 72.41  |
	| GN2v01 | FixedCutCEff_50 | 0.503 | 0.474 | 0.11619 | 0.03408 | 0.09864 | 8.61 | 29.34 | 10.14  |
	| GN2v01 | B disc cut | 0.828 | 0.05959 | 0.77664 | 0.00222 | 0.01371 | - | - | - |

	![ROC curves for light-jet, b-jet and tau-jet rejection as a function of c-tagging efficiency](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_roc_roc_DL1dv01_c_mc23.png)

	??? example "GN2v01WP: FixedCutCEff_10 "

		- INTERNAL c-jet efficiency as a function of jet pT for the FixedCutCEff_10 WP

		![c-jet efficiency as a function of jet pT for the FixedCutCEff_10 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_cjets_eff_vs_pt_wp_0p097_profile_fixed_cut_FixedCutCEff_10_mc23_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutCEff_10 WP

		![light-jet rejection as a function of jet pT for the FixedCutCEff_10 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_ujets_rej_vs_pt_wp_0p097_profile_fixed_cut_FixedCutCEff_10_mc23_.png)

		- INTERNAL b-jet rejection as a function of jet pT for the FixedCutCEff_10 WP

		![b-jet rejection as a function of jet pT for the FixedCutCEff_10 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_bjets_rej_vs_pt_wp_0p097_profile_fixed_cut_FixedCutCEff_10_mc23_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutCEff_10 WP

		![tau-jet rejection as a function of jet pT for the FixedCutCEff_10 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_taujets_rej_vs_pt_wp_0p097_profile_fixed_cut_FixedCutCEff_10_mc23_.png)

	??? example "GN2v01WP: FixedCutCEff_30 "

		- INTERNAL c-jet efficiency as a function of jet pT for the FixedCutCEff_30 WP

		![c-jet efficiency as a function of jet pT for the FixedCutCEff_30 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_cjets_eff_vs_pt_wp_0p288_profile_fixed_cut_FixedCutCEff_30_mc23_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutCEff_30 WP

		![light-jet rejection as a function of jet pT for the FixedCutCEff_30 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_ujets_rej_vs_pt_wp_0p288_profile_fixed_cut_FixedCutCEff_30_mc23_.png)

		- INTERNAL b-jet rejection as a function of jet pT for the FixedCutCEff_30 WP

		![b-jet rejection as a function of jet pT for the FixedCutCEff_30 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_bjets_rej_vs_pt_wp_0p288_profile_fixed_cut_FixedCutCEff_30_mc23_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutCEff_30 WP

		![tau-jet rejection as a function of jet pT for the FixedCutCEff_30 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_taujets_rej_vs_pt_wp_0p288_profile_fixed_cut_FixedCutCEff_30_mc23_.png)

	??? example "GN2v01WP: FixedCutCEff_50 "

		- INTERNAL c-jet efficiency as a function of jet pT for the FixedCutCEff_50 WP

		![c-jet efficiency as a function of jet pT for the FixedCutCEff_50 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_cjets_eff_vs_pt_wp_0p474_profile_fixed_cut_FixedCutCEff_50_mc23_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutCEff_50 WP

		![light-jet rejection as a function of jet pT for the FixedCutCEff_50 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_ujets_rej_vs_pt_wp_0p474_profile_fixed_cut_FixedCutCEff_50_mc23_.png)

		- INTERNAL b-jet rejection as a function of jet pT for the FixedCutCEff_50 WP

		![b-jet rejection as a function of jet pT for the FixedCutCEff_50 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_bjets_rej_vs_pt_wp_0p474_profile_fixed_cut_FixedCutCEff_50_mc23_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutCEff_50 WP

		![tau-jet rejection as a function of jet pT for the FixedCutCEff_50 WP](../../assets/r22-recommendations/pflow/GN2v01/ttbar_cjets_taujets_rej_vs_pt_wp_0p474_profile_fixed_cut_FixedCutCEff_50_mc23_.png)

=== "$Z^\prime$ mc20"

	| Name   | Efficiency operating point | Discriminant cut | c-efficiency | b-efficiency | light-flavour efficiency | tau-efficiency | b-rejection | light-flavour rejection | tau-rejection |
	| ------ | ------------------------ | ---------------- | ------------ | ------------ | ------------------------ | ----------- | ----------------------- | ----------------------- | ----------------------- |
	| GN2v01 | FixedCutCEff_10 | 3.958 | 0.026 | 0.00204 | 0.00012 | 0.00003 | 490.11 | 8559.86 | 39632.25  |
	| GN2v01 | FixedCutCEff_30 | 2.09 | 0.117 | 0.03342 | 0.00392 | 0.00045 | 29.92 | 255.26 | 2201.79  |
	| GN2v01 | FixedCutCEff_50 | 0.503 | 0.344 | 0.18605 | 0.07636 | 0.00362 | 5.37 | 13.10 | 276.18  |
	| GN2v01 | B disc cut | 0.828 | 0.05027 | 0.41043 | 0.00570 | 0.00224 | - | - | - |

	![ROC curves for light-jet, b-jet and tau-jet rejection as a function of c-tagging efficiency](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_roc_roc_DL1dv01_c_mc20.png)

	??? example "GN2v01WP: FixedCutCEff_10 "

		- INTERNAL c-jet efficiency as a function of jet pT for the FixedCutCEff_10 WP

		![c-jet efficiency as a function of jet pT for the FixedCutCEff_10 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_cjets_eff_vs_pt_wp_0p026_profile_fixed_cut_FixedCutCEff_10_mc20_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutCEff_10 WP

		![light-jet rejection as a function of jet pT for the FixedCutCEff_10 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_ujets_rej_vs_pt_wp_0p026_profile_fixed_cut_FixedCutCEff_10_mc20_.png)

		- INTERNAL b-jet rejection as a function of jet pT for the FixedCutCEff_10 WP

		![b-jet rejection as a function of jet pT for the FixedCutCEff_10 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_bjets_rej_vs_pt_wp_0p026_profile_fixed_cut_FixedCutCEff_10_mc20_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutCEff_10 WP

		![tau-jet rejection as a function of jet pT for the FixedCutCEff_10 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_taujets_rej_vs_pt_wp_0p026_profile_fixed_cut_FixedCutCEff_10_mc20_.png)

	??? example "GN2v01WP: FixedCutCEff_30 "

		- INTERNAL c-jet efficiency as a function of jet pT for the FixedCutCEff_30 WP

		![c-jet efficiency as a function of jet pT for the FixedCutCEff_30 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_cjets_eff_vs_pt_wp_0p117_profile_fixed_cut_FixedCutCEff_30_mc20_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutCEff_30 WP

		![light-jet rejection as a function of jet pT for the FixedCutCEff_30 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_ujets_rej_vs_pt_wp_0p117_profile_fixed_cut_FixedCutCEff_30_mc20_.png)

		- INTERNAL b-jet rejection as a function of jet pT for the FixedCutCEff_30 WP

		![b-jet rejection as a function of jet pT for the FixedCutCEff_30 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_bjets_rej_vs_pt_wp_0p117_profile_fixed_cut_FixedCutCEff_30_mc20_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutCEff_30 WP

		![tau-jet rejection as a function of jet pT for the FixedCutCEff_30 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_taujets_rej_vs_pt_wp_0p117_profile_fixed_cut_FixedCutCEff_30_mc20_.png)

	??? example "GN2v01WP: FixedCutCEff_50 "

		- INTERNAL c-jet efficiency as a function of jet pT for the FixedCutCEff_50 WP

		![c-jet efficiency as a function of jet pT for the FixedCutCEff_50 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_cjets_eff_vs_pt_wp_0p344_profile_fixed_cut_FixedCutCEff_50_mc20_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutCEff_50 WP

		![light-jet rejection as a function of jet pT for the FixedCutCEff_50 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_ujets_rej_vs_pt_wp_0p344_profile_fixed_cut_FixedCutCEff_50_mc20_.png)

		- INTERNAL b-jet rejection as a function of jet pT for the FixedCutCEff_50 WP

		![b-jet rejection as a function of jet pT for the FixedCutCEff_50 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_bjets_rej_vs_pt_wp_0p344_profile_fixed_cut_FixedCutCEff_50_mc20_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutCEff_50 WP

		![tau-jet rejection as a function of jet pT for the FixedCutCEff_50 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_taujets_rej_vs_pt_wp_0p344_profile_fixed_cut_FixedCutCEff_50_mc20_.png)

=== "$Z^\prime$ mc23"

	| Name   | Efficiency operating point | Discriminant cut | c-efficiency | b-efficiency | light-flavour efficiency | tau-efficiency | b-rejection | light-flavour rejection | tau-rejection |
	| ------ | ------------------------ | ---------------- | ------------ | ------------ | ------------------------ | ----------- | ----------------------- | ----------------------- | ----------------------- |
	| GN2v01 | FixedCutCEff_10 | 3.958 | 0.014 | 0.00073 | 0.00004 | 0.00002 | 1368.89 | 28278.89 | 53607.00  |
	| GN2v01 | FixedCutCEff_30 | 2.09 | 0.076 | 0.01554 | 0.00135 | 0.00030 | 64.33 | 742.01 | 3350.44  |
	| GN2v01 | FixedCutCEff_50 | 0.503 | 0.191 | 0.07500 | 0.02341 | 0.00250 | 13.33 | 42.71 | 400.05  |
	| GN2v01 | B disc cut | 0.828 | 0.06720 | 0.40722 | 0.00695 | 0.00237 | - | - | - |

	![ROC curves for light-jet, b-jet and tau-jet rejection as a function of c-tagging efficiency](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_roc_roc_DL1dv01_c_mc23.png)

	??? example "GN2v01WP: FixedCutCEff_10 "

		- INTERNAL c-jet efficiency as a function of jet pT for the FixedCutCEff_10 WP

		![c-jet efficiency as a function of jet pT for the FixedCutCEff_10 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_cjets_eff_vs_pt_wp_0p014_profile_fixed_cut_FixedCutCEff_10_mc23_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutCEff_10 WP

		![light-jet rejection as a function of jet pT for the FixedCutCEff_10 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_ujets_rej_vs_pt_wp_0p014_profile_fixed_cut_FixedCutCEff_10_mc23_.png)

		- INTERNAL b-jet rejection as a function of jet pT for the FixedCutCEff_10 WP

		![b-jet rejection as a function of jet pT for the FixedCutCEff_10 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_bjets_rej_vs_pt_wp_0p014_profile_fixed_cut_FixedCutCEff_10_mc23_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutCEff_10 WP

		![tau-jet rejection as a function of jet pT for the FixedCutCEff_10 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_taujets_rej_vs_pt_wp_0p014_profile_fixed_cut_FixedCutCEff_10_mc23_.png)

	??? example "GN2v01WP: FixedCutCEff_30 "

		- INTERNAL c-jet efficiency as a function of jet pT for the FixedCutCEff_30 WP

		![c-jet efficiency as a function of jet pT for the FixedCutCEff_30 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_cjets_eff_vs_pt_wp_0p076_profile_fixed_cut_FixedCutCEff_30_mc23_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutCEff_30 WP

		![light-jet rejection as a function of jet pT for the FixedCutCEff_30 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_ujets_rej_vs_pt_wp_0p076_profile_fixed_cut_FixedCutCEff_30_mc23_.png)

		- INTERNAL b-jet rejection as a function of jet pT for the FixedCutCEff_30 WP

		![b-jet rejection as a function of jet pT for the FixedCutCEff_30 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_bjets_rej_vs_pt_wp_0p076_profile_fixed_cut_FixedCutCEff_30_mc23_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutCEff_30 WP

		![tau-jet rejection as a function of jet pT for the FixedCutCEff_30 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_taujets_rej_vs_pt_wp_0p076_profile_fixed_cut_FixedCutCEff_30_mc23_.png)

	??? example "GN2v01WP: FixedCutCEff_50 "

		- INTERNAL c-jet efficiency as a function of jet pT for the FixedCutCEff_50 WP

		![c-jet efficiency as a function of jet pT for the FixedCutCEff_50 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_cjets_eff_vs_pt_wp_0p191_profile_fixed_cut_FixedCutCEff_50_mc23_.png)

		- INTERNAL light-jet rejection as a function of jet pT for the FixedCutCEff_50 WP

		![light-jet rejection as a function of jet pT for the FixedCutCEff_50 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_ujets_rej_vs_pt_wp_0p191_profile_fixed_cut_FixedCutCEff_50_mc23_.png)

		- INTERNAL b-jet rejection as a function of jet pT for the FixedCutCEff_50 WP

		![b-jet rejection as a function of jet pT for the FixedCutCEff_50 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_bjets_rej_vs_pt_wp_0p191_profile_fixed_cut_FixedCutCEff_50_mc23_.png)

		- INTERNAL tau-jet rejection as a function of jet pT for the FixedCutCEff_50 WP

		![tau-jet rejection as a function of jet pT for the FixedCutCEff_50 WP](../../assets/r22-recommendations/pflow/GN2v01/zprime-ext_cjets_taujets_rej_vs_pt_wp_0p191_profile_fixed_cut_FixedCutCEff_50_mc23_.png)

## Preliminary recommendation as of 07.10.2023 (superseded)

!!! warning "These recommendations are superseded"

	The following recommendations are superseded by the ones above. They are kept here for reference only.

These are preliminary recommendations for DL1d and GN2. The preliminary recommendation for GN2 is for the `GN2v00` development tagger.
You can find more information about the tagger [here](https://ftag.docs.cern.ch/algorithms/taggers/tagger_metadata/#20230306gn2v00antikt4empflow).
Because of technical limitations of the Athena b-tagging selection tool, there are two sets of operating points which are released to offer more than just the conventional four operating points for analysis teams to test.

The GN1 tagger is only released as an experimental time-stamped version which is not supported.

The recommended DL1d tagger is released under the name `DL1dv01`.
The network definition is located in `/eos/atlas/atlascerngroupdisk/asg-calib/BTagging/20220509/dl1dLoose/antikt4empflow/network.json` or [via the calibration area web interface](https://atlas-groupdata.web.cern.ch/atlas-groupdata/BTagging/20220509/dl1dLoose/antikt4empflow/network.json). 
Note that it relies on [the dips training `dipsLoose20220314v2`](https://atlas-groupdata.web.cern.ch/atlas-groupdata/BTagging/20220314/dipsLoose/antikt4empflow/network.json), which must be scheduled before `DL1dv01`.

### Working point definition for GN2v00

Discriminant defined with $f_c=0.1$.

Because the `GN2v00` algorithm provides an unprecedented discrimination power among b-jets, c-jets and light-flavour jets, it should be investigated in detail by analysis teams.
To facilitate analyses testing different GN2 OPs, allowing analyses to achieve this goal with minimal technical hurdles, two tagger names are registered in the software. They are effectively the same tagger but the OPs are defined differently:

- `GN2v00LegacyWP`: This set maintains the traditional b-tag efficiency levels of 60%, 70%, 77%, and 85%.  PCBT is defined using the same OPs. 
- `GN2v00NewAliasWP`: The OPs correspond to b-tag efficiencies of 68%, 76%, 82%, and 89%. They were derived by fixing the light-flavour jet mis-tagging efficiency to the level achieved with DL1r in rel21. PCBT is defined using the same OPs. 

Both taggers have their `fixCut` folders in the CDI file named according to the traditional convention of 60%, 70%, 77%, and 85%. However, for `GN2v00NewAliasWP`, these correspond to **68%, 76%, 82%, and 89%**, respectively.

This means an analysis only needs to register those new tagger names in the analysis framework, and does not have to add new OPs since their names remain the same. 


| Tagger name in SW | OP name         | Efficiency | Discriminant cut |
| ----------------- | --------------- | ---------- | ---------------- |
| GN2v00LegacyWP    | FixedCutBEff_60 | 60%        | 5.394            |
| GN2v00NewAliasWP  | FixedCutBEff_60 | 68%        | 4.157            |
| GN2v00LegacyWP    | FixedCutBEff_70 | 70%        | 3.875            |
| GN2v00NewAliasWP  | FixedCutBEff_70 | 76%        | 3.034            |
| GN2v00LegacyWP    | FixedCutBEff_77 | 77%        | 2.893            |
| GN2v00NewAliasWP  | FixedCutBEff_77 | 82%        | 2.145            |
| GN2v00LegacyWP    | FixedCutBEff_85 | 85%        | 1.638            |
| GN2v00NewAliasWP  | FixedCutBEff_85 | 89%        | 0.783            |


Please do refer to the TWiki [BTagRel22HighLevelSummary](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTagRel22HighLevelSummary) for further details.


### Algorithms Expected Performances in MC for GN2v00

A summary of the expected performance of `GN2v00` on PFlow jets for the different available operating points can be found in the following plots. For comparison, the performance of the `DL1dv01` and `GN120220509` taggers are overlaid.

All plots are only for internal use and not to be shown outside of the collaboration.

In addition, the c-jet and light-flavour jet rejection for the eight defined efficiency operating points is evaluated in jets from simulated ttbar events ($p_{T}$ > 20 GeV, $|\eta|$ < 2.5).

| Name   | Efficiency operating point | Discriminant cut | b-efficiency | c-efficiency | light-flavour efficiency | c-rejection | light-flavour rejection |
| ------ | ------------------------ | ---------------- | ------------ | ------------ | ------------------------ | ----------- | ----------------------- |
| GN2v00 | FixedCutBEff_60 | 5.394 | 0.60 | 0.00436 | 0.00011 | 229.36 | 9473.81 |
| GN2v00 | FixedCutBEff_68 | 4.157 | 0.68 | 0.01652 | 0.00037 | 60.54 | 2689.21 |
| GN2v00 | FixedCutBEff_70 | 3.875 | 0.70 | 0.02281 | 0.00053 | 43.84 | 1902.75 |
| GN2v00 | FixedCutBEff_76 | 3.034 | 0.76 | 0.05763 | 0.00152 | 17.35 | 658.83 |
| GN2v00 | FixedCutBEff_77 | 2.893 | 0.77 | 0.06672 | 0.00183 | 14.99 | 547.03 |
| GN2v00 | FixedCutBEff_82 | 2.145 | 0.82 | 0.13619 | 0.00479 | 7.34 | 208.65 |
| GN2v00 | FixedCutBEff_85 | 1.638 | 0.85 | 0.20418 | 0.00893 | 4.90 | 111.96 |
| GN2v00 | FixedCutBEff_89 | 0.783 | 0.89 | 0.33801 | 0.02338 | 2.96 | 42.76 |



- INTERNAL light-jet and c-jet rejection as a function of b-tagging efficiency

![ROC curves for light-jet and c-jet rejection as a function of b-tagging efficiency](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_roc_roc_GN2v00_b.png)

??? example "GN2v00LegacyWP: FixedCutBEff_60 (60% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 60% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 60% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_60.png)

    - INTERNAL light jet rejection as a function of jet pT for 60% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 60% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_60.png)

    - INTERNAL c jet rejection as a function of jet pT for 60% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 60% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_60.png)

??? example "GN2v00NewAliasWP: FixedCutBEff_60 (68% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 68% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 68% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_68.png)

    - INTERNAL light jet rejection as a function of jet pT for 68% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 68% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_68.png)

    - INTERNAL c jet rejection as a function of jet pT for 68% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 68% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_68.png)

??? example "GN2v00LegacyWP: FixedCutBEff_70 (70% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 70% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 70% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_70.png)

    - INTERNAL light jet rejection as a function of jet pT for 70% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 70% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_70.png)

    - INTERNAL c jet rejection as a function of jet pT for 70% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 70% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_70.png)

??? example "GN2v00NewAliasWP: FixedCutBEff_70 (76% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 76% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 76% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_76.png)

    - INTERNAL light jet rejection as a function of jet pT for 76% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 76% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_76.png)

    - INTERNAL c jet rejection as a function of jet pT for 76% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 76% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_76.png)

??? example "GN2v00LegacyWP: FixedCutBEff_77 (77% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 77% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 77% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_77.png)

    - INTERNAL light jet rejection as a function of jet pT for 77% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 77% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_77.png)

    - INTERNAL c jet rejection as a function of jet pT for 77% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 77% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_77.png)

??? example "GN2v00NewAliasWP: FixedCutBEff_77 (82% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 82% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 82% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_82.png)

    - INTERNAL light jet rejection as a function of jet pT for 82% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 82% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_82.png)

    - INTERNAL c jet rejection as a function of jet pT for 82% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 82% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_82.png)

??? example "GN2v00LegacyWP: FixedCutBEff_85 (85% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 85% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 85% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_85.png)

    - INTERNAL light jet rejection as a function of jet pT for 85% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 85% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_85.png)

    - INTERNAL c jet rejection as a function of jet pT for 85% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 85% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_85.png)

??? example "GN2v00NewAliasWP: FixedCutBEff_85 (89% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 89% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 89% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_89.png)

    - INTERNAL light jet rejection as a function of jet pT for 89% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 89% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_89.png)

    - INTERNAL c jet rejection as a function of jet pT for 89% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 89% WP](../../assets/r22-recommendations/pflow/GN2v00/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_89.png)


### Working point definition for DL1dv01

Discriminant defined with $f_c=0.018$.

| Efficiency | Discriminant cut |
| ---------- | ---------------- |
| 60%        | 4.854            |
| 70%        | 3.493            |
| 77%        | 2.456            |
| 85%        | 0.948            |

### Algorithms Expected Performances in MC for DL1dv01

A summary of the expected performance of `DL1dv01` on PFlow jets for the different available working points can be found in the following plots. For comparison, the performance of the `DL1dv00` and `DL1r` (retrained in r22 for comparison) taggers are overlaid.

All plots are only for internal use and not to be shown outside of the collaboration.

In addition, the c-jet and light-flavour jet rejection for the four defined efficiency operating points is evaluated in jets from simulated ttbar events ($p_{T}$ > 20 GeV, $|\eta|$ < 2.5).

| Name   | Efficiency operating point | Discriminant cut | b-efficiency | c-efficiency | light-flavour efficiency | c-rejection | light-flavour rejection |
| ------ | ------------------------ | ---------------- | ------------ | ------------ | ------------------------ | ----------- | ----------------------- |
| DL1dv01 | FixedCutBEff_60 | 4.854 | 0.60 | 0.01902 | 0.00024 | 52.57 | 4175.17 |
| DL1dv01 | FixedCutBEff_70 | 3.493 | 0.70 | 0.06937 | 0.00113 | 14.41 | 883.32 |
| DL1dv01 | FixedCutBEff_77 | 2.456 | 0.77 | 0.15542 | 0.00380 | 6.43 | 263.50 |
| DL1dv01 | FixedCutBEff_85 | 0.948 | 0.85 | 0.32864 | 0.01916 | 3.04 | 52.20 |


- INTERNAL light-jet and c-jet rejection as a function of b-tagging efficiency

![ROC curves for light-jet and c-jet rejection as a function of b-tagging efficiency](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_roc_roc_DL1dv01_b.png)

??? example "FixedCutBEff_60 (60% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 60% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 60% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_60.png)

    - INTERNAL light jet rejection as a function of jet pT for 60% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 60% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_60.png)

    - INTERNAL c jet rejection as a function of jet pT for 60% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 60% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_60.png)

??? example "FixedCutBEff_70 (70% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 70% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 70% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_70.png)

    - INTERNAL light jet rejection as a function of jet pT for 70% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 70% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_70.png)

    - INTERNAL c jet rejection as a function of jet pT for 70% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 70% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_70.png)

??? example "FixedCutBEff_77 (77% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 77% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 77% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_77.png)

    - INTERNAL light jet rejection as a function of jet pT for 77% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 77% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_77.png)

    - INTERNAL c jet rejection as a function of jet pT for 77% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 77% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_77.png)

??? example "FixedCutBEff_85 (85% b-tagging efficiency)"

    - INTERNAL b-jet efficiency as a function of jet pT for 85% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 85% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_bjets_eff_vs_pt_profile_fixed_cut__FixedCutBEff_85.png)

    - INTERNAL light jet rejection as a function of jet pT for 85% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 85% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_ujets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_85.png)

    - INTERNAL c jet rejection as a function of jet pT for 85% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 85% WP](../../assets/r22-recommendations/pflow/DL1dv01/ttbar_bjets_cjets_rej_vs_pt_profile_fixed_cut__FixedCutBEff_85.png)


## Preliminary recommendation as of 27.06.2022 (superseded)

??? example "Working point definition for DL1dv01"

    Discriminant defined with $f_c=0.018$.

    | Efficiency | Discriminant cut |
    | ---------- | ---------------- |
    | 60%        | 4.854            |
    | 70%        | 3.493            |
    | 77%        | 2.456            |
    | 85%        | 0.948            |

??? example "Algorithms Expected Performances in MC for DL1dv01"

    A summary of the expected performance of `DL1dv01` on PFlow jets for the different available working points can be found in the following plots. For comparison, the performance of the `DL1dv00` and `DL1r` tagger (r21 recommendation - i.e. trained in r21) is overlaid.

    All plots are only for internal use and not to be shown outside of the collaboration.

    - INTERNAL light-jet and c-jet rejection as a function of b-tagging efficiency

    ![ROC curves for light-jet and c-jet rejection as a function of b-tagging efficiency](../../assets/r22-preliminary-recommendations/roc_DL1dv01_ttbar.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 60% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 60% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_b_eff_FixedCutBEff_60_ttbar.png)

    - INTERNAL light jet rejection as a function of jet pT for 60% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 60% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_light_rej_FixedCutBEff_60_ttbar.png)

    - INTERNAL c jet rejection as a function of jet pT for 60% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 60% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_c_rej_FixedCutBEff_60_ttbar.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 70% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 70% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_b_eff_FixedCutBEff_70_ttbar.png)

    - INTERNAL light jet rejection as a function of jet pT for 70% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 70% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_light_rej_FixedCutBEff_70_ttbar.png)

    - INTERNAL c jet rejection as a function of jet pT for 70% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 70% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_c_rej_FixedCutBEff_70_ttbar.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 77% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 77% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_b_eff_FixedCutBEff_77_ttbar.png)

    - INTERNAL light jet rejection as a function of jet pT for 77% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 77% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_light_rej_FixedCutBEff_77_ttbar.png)

    - INTERNAL c jet rejection as a function of jet pT for 77% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 77% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_c_rej_FixedCutBEff_77_ttbar.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 85% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 85% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_b_eff_FixedCutBEff_85_ttbar.png)

    - INTERNAL light jet rejection as a function of jet pT for 85% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 85% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_light_rej_FixedCutBEff_85_ttbar.png)

    - INTERNAL c jet rejection as a function of jet pT for 85% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 85% WP](../../assets/r22-preliminary-recommendations/eff_DL1dv01_pt_c_rej_FixedCutBEff_85_ttbar.png)


## Experimental recommendation as of 27.06.2022  (superseded)

The GN1 tagger (timestamp: `GN120220509`) is released in derivations for experimental testing of analysis teams. The network definition is located in `/eos/atlas/atlascerngroupdisk/asg-calib/BTagging/20220509/gn1/antikt4empflow/network.onnx` or [via the calibration area web interface](https://atlas-groupdata.web.cern.ch/atlas-groupdata/BTagging/20220509/gn1/antikt4empflow/network.onnx).

The GN1 tagger is superseded by the GN2 tagger with version `GN2v00`.

??? example "Working point definition for GN120220509"

    Discriminant defined with $f_c=0.05$.

    | Efficiency | Discriminant cut |
    | ---------- | ---------------- |
    | 60%        | 5.135            |
    | 70%        | 3.642            |
    | 77%        | 2.602            |
    | 85%        | 1.253            |

??? example "Algorithms Expected Performances in MC for GN120220509"

    A summary of the expected performance of `GN120220509` on PFlow jets for the different available working points can be found in the following plots. For comparison, the performance of the `DL1dv01` and `DL1r` tagger (r21 recommendation - i.e. trained in r21) is overlaid.

    All plots are only for internal use and not to be shown outside of the collaboration.

    - INTERNAL light-jet and c-jet rejection as a function of b-tagging efficiency

    ![ROC curves for light-jet and c-jet rejection as a function of b-tagging efficiency](../../assets/r22-preliminary-recommendations/roc_GN120220509_ttbar.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 60% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 60% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_b_eff_FixedCutBEff_60_ttbar.png)

    - INTERNAL light jet rejection as a function of jet pT for 60% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 60% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_light_rej_FixedCutBEff_60_ttbar.png)

    - INTERNAL c jet rejection as a function of jet pT for 60% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 60% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_c_rej_FixedCutBEff_60_ttbar.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 70% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 70% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_b_eff_FixedCutBEff_70_ttbar.png)

    - INTERNAL light jet rejection as a function of jet pT for 70% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 70% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_light_rej_FixedCutBEff_70_ttbar.png)

    - INTERNAL c jet rejection as a function of jet pT for 70% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 70% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_c_rej_FixedCutBEff_70_ttbar.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 77% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 77% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_b_eff_FixedCutBEff_77_ttbar.png)

    - INTERNAL light jet rejection as a function of jet pT for 77% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 77% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_light_rej_FixedCutBEff_77_ttbar.png)

    - INTERNAL c jet rejection as a function of jet pT for 77% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 77% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_c_rej_FixedCutBEff_77_ttbar.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 85% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 85% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_b_eff_FixedCutBEff_85_ttbar.png)

    - INTERNAL light jet rejection as a function of jet pT for 85% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 85% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_light_rej_FixedCutBEff_85_ttbar.png)

    - INTERNAL c jet rejection as a function of jet pT for 85% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 85% WP](../../assets/r22-preliminary-recommendations/eff_GN120220509_pt_c_rej_FixedCutBEff_85_ttbar.png)


## Preliminary recommendation as of 08.10.2021 (superseded)


The name of this tagger is `DL1dv00` and the network definition is located in `/eos/atlas/atlascerngroupdisk/asg-calib/BTagging/20210824r22/dl1dLoose/antikt4empflow/network.json` or [via the calibration area web interface](https://atlas-groupdata.web.cern.ch/atlas-groupdata/BTagging/20210824r22/dl1dLoose/antikt4empflow/network.json). 
Note that it relies on [the dips training `dipsLoose20210729`](https://atlas-groupdata.web.cern.ch/atlas-groupdata/BTagging/20210729/dipsLoose/antikt4empflow/network.json), which must be scheduled before `DL1dv00`.

The DL1d tagger with version `DL1dv00` is superseded by the DL1d tagger with version `DL1dv01`.


??? example "Working point definition for DL1dv00"

    Discriminant defined with $f_c=0.018$.

    | Efficiency | Discriminant cut |
    | ---------- | ---------------- |
    | 60%        | 4.884            |
    | 70%        | 3.494            |
    | 77%        | 2.443            |
    | 85%        | 0.930            |

??? example "Algorithms Expected Performances in MC for DL1dv00"

    A summary of the expected performance of `DL1dv00` (preliminary recommendation- trained on r22) on PFlow jets for the different available working points can be found in the following plots.
    For comparison, the performance of the `DL1r` tagger (r21 recommendation - i.e. trained in r21) is overlaid.

    All plots are only for internal use and not to be shown outside of the collaboration.

    - INTERNAL light-jet and c-jet rejection as a function of b-tagging efficiency

    ![ROC curves for light-jet and c-jet rejection as a function of b-tagging efficiency](../../assets/r22-preliminary-recommendations/DL1dloosev00_DL1r_ttbar_r22_0.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 60% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 60% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_bjets_60eff_ttbar_r22_0.png)

    - INTERNAL light jet rejection as a function of jet pT for 60% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 60% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_ujets_60eff_ttbar_r22_0.png)

    - INTERNAL c jet rejection as a function of jet pT for 60% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 60% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_cjets_60eff_ttbar_r22_0.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 70% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 70% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_bjets_70eff_ttbar_r22_0.png)

    - INTERNAL light jet rejection as a function of jet pT for 70% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 70% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_ujets_70eff_ttbar_r22_0.png)

    - INTERNAL c jet rejection as a function of jet pT for 70% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 70% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_cjets_70eff_ttbar_r22_0.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 77% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 77% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_bjets_77eff_ttbar_r22_0.png)

    - INTERNAL light jet rejection as a function of jet pT for 77% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 77% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_ujets_77eff_ttbar_r22_0.png)

    - INTERNAL c jet rejection as a function of jet pT for 77% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 77% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_cjets_77eff_ttbar_r22_0.png)


    - INTERNAL b-jet efficiency as a function of jet pT for 85% b-tagging efficiency WP

    ![b-jet efficiency as a function of jet pT for 85% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_bjets_85eff_ttbar_r22_0.png)

    - INTERNAL light jet rejection as a function of jet pT for 85% b-tagging efficiency WP

    ![light jet rejection as a function of jet pT for 85% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_ujets_85eff_ttbar_r22_0.png)

    - INTERNAL c jet rejection as a function of jet pT for 85% b-tagging efficiency WP

    ![c jet rejection as a function of jet pT for 85% WP](../../assets/r22-preliminary-recommendations/DL1dloosev00_pT_vs_beff_cjets_85eff_ttbar_r22_0.png)
