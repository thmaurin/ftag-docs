The tracks are decorated with truth information by the `FlavorTagDiscriminants::TrackTruthDecoratorAlg` ([`TrackTruthDecoratorAlg.h`](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/FlavorTagDiscriminants/TrackTruthDecoratorAlg.h) | [`TrackTruthDecoratorAlg.cxx`](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/src/TrackTruthDecoratorAlg.cxx)).

The truth particles which are used to decorate tracks with truth information are decorated by the `FlavorTagDiscriminants::TruthParticleDecoratorAlg` ([`TruthParticleDecoratorAlg.h`](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/FlavorTagDiscriminants/TruthParticleDecoratorAlg.h) | [`TruthParticleDecoratorAlg.cxx`](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/src/TruthParticleDecoratorAlg.cxx))

## Origin labels

Apart from the jet labelling, also track truth origin labels are available.
This label is obtained by examining the full ancestry of the truth particle linked to the track.
The corresponding variable is `ftagTruthOriginLabel` which is defined [here](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/TrackingID/InDetTrackSystematicsTools/InDetTrackSystematicsTools/InDetTrackTruthOriginDefs.h#L137).

| ftagTruthOriginLabel | Category         | Description
| -------------------- | ---------------- |  ---------------- |
| 0                    | pile-up          |  From a 𝑝𝑝 collision other than the primary interaction|
| 1                    | Fake             |  Created from the hits of multiple particles |
| 2                    | Primary          |  Does not originate from any secondary decay (labels 3-7)|
| 3                    | FromB            |  From the decay of a 𝑏-hadron|
| 4                    | FromBC           |  From a 𝑐-hadron decay, which itself is from the decay of a 𝑏-hadron |
| 5                    | FromC            |  From the decay of a 𝑐-hadron |
| 6                    | FromTau          |  From the decay of a 𝜏 |
| 7                    | OtherSecondary   |  From other secondary interactions |


## Type labels

Additionally, the truth type of the track itself is available.
This label is obtained by examining the truth particle linked to the track.
The corresponding variable is `ftagTruthTypeLabel` which is defined [here](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/FlavorTagDiscriminants/TruthDecoratorHelpers.h#L14).

For the values `ftagTruthTypeLabel > 1`, the sign of the value denotes the charge of the truth particle.

| ftagTruthTypeLabel | Category         | Description
| ------------------ | ---------------- |  ---------------- |
| 0                  | NoTruth          | no association with truth particle possible (e.g. PU) |
| 1                  | Other            | associated with truth particle but none of the types below |
| 2                  | Pion             | track associated with pion (PDG ID: 211) |
| 3                  | Kaon             | track associated with kaon |
| 4                  | Lambda           | track associated with lambda (PDG ID: 3122) |
| 5                  | Electron         | track associated with electron |
| 6                  | Muon             | track associated with muon |
| 7                  | Photon           | track associated with photon |


## Source labels

You can also obtain information about the immediate parent particle/process of the track with the track's source label.
This label is obtained by examining the parent of the truth particle linked to the track.
The corresponding variable is `ftagTruthSourceLabel` which is defined [here](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/FlavorTagDiscriminants/TruthDecoratorHelpers.h#L27).

| ftagTruthSourceLabel | Category         | Description
| ------------------ | ---------------- |  ---------------- |
| 0                  | NoTruth          | no association with truth particle possible (e.g. PU) |
| 1                  | Other            | associated with truth particle but none of the types below |
| 2                  | KaonDecay        | from the decay of a kaon |
| 3                  | LambdaDecay      | from the decay of a lambda (PDG ID: 3122) |
| 4                  | Conversion       | from the conversion of a photon |


## Vertex labels

`ftagTruthVertexIndex` truth vertex index of the track. 

* 0 is reserved for the truth PV (primary vertex) 
* any SVs are indexed arbitrarily with an int `>0` 
* Truth vertices within 0.1mm are merged.
* the label `-2` indicates that there is no truth link or truth vertex present (mostly for pile-up tracks)
