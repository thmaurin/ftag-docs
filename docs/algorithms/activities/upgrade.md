# Upgrade activities

## Producing h5 ntuples from upgrade samples

The recommended way to process the samples for upgrade studies is using the [training-dataset-dumper](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/). Instructions on how to use it for processing upgrade AODs are provided [here](https://training-dataset-dumper.docs.cern.ch/advanced_usage/#running-over-r219-upgrade-aods).


## Recommended h5 ntuples for upgrade studies


**ttbar (14 TeV, Phase-2 geometry)**

dumped from `mc21_14TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.deriv.DAOD_FTAG1.e8481_s4149_r14700_r14702_p5799`:
```
user.jcrosby.601229.e8481_s4149_r14700_r14702_p5799.tdd.upgrade_r24.24_0_17.23-11-28_700_output.h5
```
dumped from `mc21_14TeV.601230.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_FTAG1.e8481_s4149_r14700_r14702_p5799`:
```
user.jcrosby.601230.e8481_s4149_r14700_r14702_p5799.tdd.upgrade_r24.24_0_17.23-11-26_1402_output.h5
```

**Z' (14 TeV, Phase-2 geometry)**

dumped from `mc21_14TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e8481_s4149_r14700_r14702_p5799`

```
user.jcrosby.800030.e8481_s4149_r14700_r14702_p5799.tdd.upgrade_r24.24_0_16.23-11-15_1356_output.h5/
```

## Recommended samples for upgrade studies

| sample | pileup | AOD  | comments |
| ------ | ------ | ---- |--------- |
| ttbar  | 200.   | mc21_14TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.merge.AOD.e8481_s4149_r14700_r14702 | 50M events (high stats sample) r24 |
| ttbar. | 200.   | mc21_14TeV.601230.PhPy8EG_A14_ttbar_hdamp258p75_dil.merge.AOD.e8481_s4149_r14700_r14702 | 50M events (high stats sample) r24 |
| Z'.    | 200.   | mc21_14TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.merge.AOD.e8481_s4149_r14700_r14702 | 30M events (high stats sample) r24 |
| ttbar  | 200    | mc21_14TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.recon.AOD.e8481_s4149_r14700 | 50M events (high stats samples) r21.9 |
| Z'     | 200    | mc21_14TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.recon.AOD.e8481_s4149_r14700 | 20M events (high stats samples) r21.9 |
| ttbar  | 200    | mc15_14TeV.600012.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.recon.AOD.e8185_s3654_s3657_r12573 | with PU truth |
| ttbar  | 200    | mc15_14TeV.600012.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.recon.AOD.e8185_s3770_s3773_r13618 | for HGTD studies |
| Z'     | 200    | mc15_14TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.recon.AOD.e8185_s3654_s3657_r12574 | standard truth |
| single top | 200 | mc15_14TeV.601351.PhPy8EG_tqb_lep_top.recon.AOD.e8470_s3770_s3773_r13619 | for HGTD studies (enriched in high eta region) |
| single anti-top | 200 | mc15_14TeV.601350.PhPy8EG_tqb_lep_antitop.recon.AOD.e8470_s3770_s3773_r13619 | for HGTD studies (enriched in high eta region) |
| ttbar  | 0      | mc15_14TeV.600012.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.recon.AOD.e8185_s3654_s3657_r12440 | |
| ttbar  | 0      | mc15_14TeV.600012.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.recon.AOD.e8185_s3770_s3773_r13617 | for HGTD studies |
| Z'     | 0      | mc15_14TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.recon.AOD.e8185_s3654_s3657_r12440 | |




## Available models
The trainings on upgrade samples for Dips, DL1d and GN1 (made with tight tracks selection) are now available [here](https://ftag.docs.cern.ch/algorithms/available_taggers/#upgrade-run4).
