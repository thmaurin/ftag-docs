# Trackless-b-tagging activities

## Introduction

Track-less b-tagging uses information about hits (or their absence) on detector layers to infer the presence of b-jets.

## Reconstruction tags 

We have used the following r-tags for AODs:
### mc23d (latest samples)
- [`r15310`](https://ami.in2p3.fr/?subapp=tagsShow&userdata=r15310): saves hits with dR(hit,jet)<0.2 and pt(jet)>250 GeV (used for Z')
- [`r15328`](https://ami.in2p3.fr/?subapp=tagsShow&userdata=r15328): saves hits with dR(hit,jet)<0.2 and pt(jet)>20 GeV (used for ttbar)


### mc20 (latest samples)
- [`r13258`](https://ami.in2p3.fr/?subapp=tagsShow&userdata=r13258): saves hits with dR(hit,jet)<0.4 and pt(jet)>300 GeV
- [`r14736`](https://ami.in2p3.fr/?subapp=tagsShow&userdata=r14736): saves hits with dR(hit,jet)<0.4 and pt(jet)>20 GeV


## Derivation tags 

We have used the following p-tags for DAODs:

- [`p6023`](https://ami.in2p3.fr/?subapp=tagsShow&userdata=p6023): should be used for trackless AODs

## Recommended samples for trackless-b-tagging studies

| name        | h5     | DxAOD  | AOD      | comments |
| ----------- | ------ | ------ |--------- | -------- |
| extended Z' | `user.rkusters.800030.e8514_e8528_s4159_s4114_r15310_r15319_p6023.tdd.TracklessEMPFlow.24_2_37.24-03-05_mc23d_output.h5` | `mc23_13p6TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.merge.DAOD_FTAG1.e8514_e8528_s4159_s4114_r15310_r15319_p6023` | `mc23_13p6TeV:mc23_13p6TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.recon.AOD.e8514_s4159_r15310` | Hits in h5 files are saved in a cone of dR=0.1 around the jet axis. |
| ttbar | `user.rkusters.601589.e8549_e8528_s4159_s4114_r15328_p6023.tdd.TracklessEMPFlow.24_2_37.24-03-07_mc23d_output.h5` | `mc23_13p6TeV.601589.PhPy8EG_A14_ttbar_hdamp258p75_nonallhadron.merge.DAOD_FTAG1.e8549_e8528_s4159_s4114_r15328_p6023`| `mc23_13p6TeV.601589.PhPy8EG_A14_ttbar_hdamp258p75_nonallhadron.merge.DAOD_FTAG1.e8549_e8528_s4159_s4114_r15328` | Hits in h5 files are saved in a cone of dR=0.1 around the jet axis. |


You can download the datasets from the grid using `rucio`: [ATLAS software tutorial about grid downloads with `rucio`](https://atlassoftwaredocs.web.cern.ch/AnalysisSWTutorial/rucio_download_files/).

## Information for Plotting
Some scripts exist for the plotting of Hit/Track Variables. They are located in a [git repo](https://gitlab.cern.ch/rkusters/hitscripts). Feedback/Comments/Ideas are appreciated!