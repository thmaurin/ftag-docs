# MC/MC efficiency maps

The default efficiency of flavour tagging algorithms in Monte Carlo (MC) simulation is parametrised in terms of the "Powheg + Pythia 8" ttbar samples. 
It has been observed that for alternative generators, the calibrations have become sufficiently precise as to be sensitive to the implementation of parton showering and hadron decay modelling in various MC event generators. Consequently, there are different flavour tagging efficiencies in MC for these different generators.

Assuming that the effects of these on the MC efficiencies factorise from other effects, for a number of generator choices special reference histograms are provided which are called MC efficiency maps. They allow for providing the alternative performance with so-called MC/MC scale factors.

The MC/MC scale factors are always computed as "Pythia8 Samples / Alternative Sample".
The rationale behind this is that while the default MC efficiency maps are used as the denominator for the scale factor computation

$$ SF = \frac{eff_{data}}{eff_{MC}},$$

the additional MC/MC scale factor can be computed as the ratio of efficiencies in the references for the calibration scale factor object and the MC efficiency map, respectively. Consequently, the rescaled scale factor for alternative MC event generators is defined as 

$$ SF_{rescaled} = \frac{eff_{data}}{eff_{MC, reference}} \cdot \frac{eff_{MC, reference}}{eff_{MC, alternative}}.$$

An instructive overview about the concept of MC/MC scale factors is provided in this talk by Yusong Tian: [Overview of MC-to-MC scale factors in Jet tagging and scale factor meeting (29 Apr 2021)](https://indico.cern.ch/event/1031681/#9-overview-of-mc-to-mc-scale-f).


## MC/MC efficiency map software

The software to compute MC/MC efficiency maps and create a CDI file is provided here:

- [https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/efficiency-maps](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/efficiency-maps)


## List of generators and samples

??? info "For more information on samples see [here](https://ftag.docs.cern.ch/samples/sample_list/)"

### Release 22 supported generators

No support is provided for Sherpa 2.2.8. Instead, users are urged towards using samples with a more recent Sherpa version.

No support is provided for Powheg + Herwig 7.0.4. Instead users are urged towards using samples with a more recent version of Herwig.

Notation: `410250-2` means DSIDs from 410250 to 410252.

#### Run 2

- `410470`: Powheg + Pythia8 + EvtGen on ttbar
- `410250`: Sherpa 2.2.1 on ttbar
- `410464`: amc@NLO + Pythia8 + EvtGen on ttbar
- `412116`: amc@NLO+Herwig on ttbar 
- `411233`: Powheg + Herwig 7.1.3 + EvtGen on ttbar
- `600666`: Powheg + Herwig 7.2.1 + EvtGen on ttbar (can be used as well for Herwig 7.2.3)
- `700122`: Sherpa 2.2.10 on ttbar (can be used as well for Sherpa 2.2.11 and 2.2.12)
- `700660`: Sherpa 2.2.12 on ttbar 

#### Run 3

- `601229`: Powheg + Pythia8 + EvtGen on ttbar (hdamp258p75)
- `601414`: Powheg + Herwig 7 + EvtGen on ttbar
- `700660`: Sherpa 2.2.12 on ttbar


### Release 21 supported generators

MC/MC scale factors in release 21 are included in the CDI since February 2018. The efficiency maps used for constructing the MC/MC scale factors are below.

- `"410470"`: Powheg + Pythia8 + EvtGen on ttbar
- `"410250"`: Sherpa 2.2.1 on ttbar
- `"410558"`: Powheg + Herwig 7.0.4 + EvtGen on ttbar
- `"410464"`: amc@NLO + Pythia8 + EvtGen on ttbar
- `"411233"`: Powheg + Herwig 7.1.3 + EvtGen on ttbar
- `"421152"`: Sherpa 2.2.8 on ttbar
- `"600666"`: Powheg + Herwig 7.2.1 + EvtGen on ttbar
- `"700122"`: Sherpa 2.2.10 on ttbar

??? info "Sherpa 2.2.11 and 2.2.12 efficiency maps"

    Currently, no efficiency maps are provided for Sherpa 2.2.11 MC and Sherpa 2.2.12 MC samples. Only the mc16a campaign of the Sherpa 2.2.11 tt+jets sample has been generated. Based on checks of the Sherpa 2.2.11 mc16a and the mc16a+mc16d+mc16e Sherpa 2.2.12 MC samples, differences in the MC/MC efficiency w.r.t. the Sherpa 2.2.10 samples are small.

    If you need precision efficiency parametrisation for your analysis, please get in touch with the FTAG algorithms conveners to coordinate requesting the missing mc16d and mc16e samples and the study of efficiency maps.
    Otherwise, consider using the efficiency maps for Sherpa 2.2.10. [It has been observed](https://indico.cern.ch/event/1087283/#3-mc-efficiency-map-sherpa-221) that the efficiency between Sherpa 2.2.11 and Sherpa 2.2.10 is quite similar.
    [Similar studies](https://indico.cern.ch/event/1174053/#3-sherpa-2212-efficiency-maps) demonstrate similarity between Sherpa 2.2.12 and Sherpa 2.2.10.




## List of samples used for MC/MC efficiency map computation

Below is a non-exhaustive list of MC samples used to compute the MC efficiencies of alternative generators.

### r22 samples

#### 13.6 TeV samples (mc21 / Run-3)

- Powheg + Pythia 8 (`"601229"`)
	```
	mc21_13p6TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.deriv.DAOD_PHYS.e8453_s3873_r13829_p5631
	mc21_13p6TeV.601230.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_PHYS.e8453_s3873_r13829_p5631
	```

- Sherpa 2.2.12 (`"700660"`)
	```
	mc21_13p6TeV.700660.Sh_2212_ttbar_dilepton_maxHTavrgTopPT.deriv.DAOD_PHYS.e8462_e8455_s3873_s3874_r13829_r13831_p5631
	mc21_13p6TeV.700661.Sh_2212_ttbar_SingleLeptonM_maxHTavrgTopPT.deriv.DAOD_PHYS.e8462_e8455_s3873_s3874_r13829_r13831_p5631
	mc21_13p6TeV.700662.Sh_2212_ttbar_SingleLeptonP_maxHTavrgTopPT.deriv.DAOD_PHYS.e8462_e8455_s3873_s3874_r13829_r13831_p5631
	```

- Powheg + Herwig 7.2.3 + EvtGen (`"601414"`)
	```
	mc21_13p6TeV.601414.PhH7EG_A14_ttbar_hdamp258p75_Single_Lep.deriv.DAOD_PHYS.e8472_e8455_s3873_s3874_r13829_r13831_p5631
	mc21_13p6TeV.601415.PhH7EG_A14_ttbar_hdamp258p75_Dilep.deriv.DAOD_PHYS.e8472_e8455_s3873_s3874_r13829_r13831_p5631
	```


#### 13 TeV samples (mc20 / Run-2)

- Powheg + Pythia 8 (`"410470"`)
	```
	mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_PHYS.e6337_s3681_r13144_p5631
	mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_PHYS.e6337_s3681_r13145_p5631
	mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_PHYS.e6337_s3681_r13167_p5631
	```

	There is also a version of this sample in fast simulation. It allows to check if differences between full Geant4 simulation and fast simulation are picked up by the algorithms.

	```
	mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_PHYS.e6337_a899_r13144_p5631
	mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_PHYS.e6337_a899_r13145_p5631
	mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_PHYS.e6337_a899_r13167_p5631
	```

- Sherpa 2.2.10 (`"700122"`)
	```
	mc20_13TeV.700122.Sh_2210_ttbar_SingleLeptonP_maxHTavrgTopPT_SSC.deriv.DAOD_PHYS.e8253_s3681_r13145_p5631
	mc20_13TeV.700122.Sh_2210_ttbar_SingleLeptonP_maxHTavrgTopPT_SSC.deriv.DAOD_PHYS.e8253_s3681_r13144_p5631
	mc20_13TeV.700122.Sh_2210_ttbar_SingleLeptonP_maxHTavrgTopPT_SSC.deriv.DAOD_PHYS.e8253_s3681_r13167_p5631

	mc20_13TeV.700123.Sh_2210_ttbar_SingleLeptonM_maxHTavrgTopPT_SSC.deriv.DAOD_PHYS.e8253_s3681_r13144_p5631
	mc20_13TeV.700123.Sh_2210_ttbar_SingleLeptonM_maxHTavrgTopPT_SSC.deriv.DAOD_PHYS.e8253_s3681_r13145_p5631
	mc20_13TeV.700123.Sh_2210_ttbar_SingleLeptonM_maxHTavrgTopPT_SSC.deriv.DAOD_PHYS.e8253_s3681_r13167_p5631

	mc20_13TeV.700124.Sh_2210_ttbar_dilepton_maxHTavrgTopPT_SSC.deriv.DAOD_PHYS.e8253_s3681_r13144_p5631
	mc20_13TeV.700124.Sh_2210_ttbar_dilepton_maxHTavrgTopPT_SSC.deriv.DAOD_PHYS.e8253_s3681_r13145_p5631
	mc20_13TeV.700124.Sh_2210_ttbar_dilepton_maxHTavrgTopPT_SSC.deriv.DAOD_PHYS.e8253_s3681_r13167_p5631
	```

- aMC@NLO + Pythia 8 (`"410464"`)
	```
	mc20_13TeV.410464.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_SingleLep.deriv.DAOD_PHYS.e6762_a899_r13144_p5631
	mc20_13TeV.410464.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_SingleLep.deriv.DAOD_PHYS.e6762_a899_r13145_p5631
	mc20_13TeV.410464.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_SingleLep.deriv.DAOD_PHYS.e6762_a899_r13167_p5631

	mc20_13TeV.410465.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_dil.deriv.DAOD_PHYS.e6762_a899_r13144_p5631
	mc20_13TeV.410465.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_dil.deriv.DAOD_PHYS.e6762_a899_r13145_p5631
	mc20_13TeV.410465.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_dil.deriv.DAOD_PHYS.e6762_a899_r13167_p5631
	```

- Powheg + Herwig 7.1.3 + EvtGen (`"411233"`)
	```
	mc20_13TeV.411233.PowhegHerwig7EvtGen_tt_hdamp258p75_713_SingleLep.deriv.DAOD_PHYS.e7580_a899_r13144_p5631
	mc20_13TeV.411233.PowhegHerwig7EvtGen_tt_hdamp258p75_713_SingleLep.deriv.DAOD_PHYS.e7580_a899_r13145_p5631
	mc20_13TeV.411233.PowhegHerwig7EvtGen_tt_hdamp258p75_713_SingleLep.deriv.DAOD_PHYS.e7580_a899_r13167_p5631

	mc20_13TeV.411234.PowhegHerwig7EvtGen_tt_hdamp258p75_713_dil.deriv.DAOD_PHYS.e7580_a899_r13144_p5631
	mc20_13TeV.411234.PowhegHerwig7EvtGen_tt_hdamp258p75_713_dil.deriv.DAOD_PHYS.e7580_a899_r13145_p5631
	mc20_13TeV.411234.PowhegHerwig7EvtGen_tt_hdamp258p75_713_dil.deriv.DAOD_PHYS.e7580_a899_r13167_p5631
	```

- aMcAtNlo + Herwig7.1.3 + EvtGen (`"412116"`)
	```
	mc20_13TeV.412116.aMcAtNloHerwig7EvtGen_MEN30NLO_ttbar_SingleLep.deriv.DAOD_PHYS.e7620_a899_r13144_p5631
	mc20_13TeV.412116.aMcAtNloHerwig7EvtGen_MEN30NLO_ttbar_SingleLep.deriv.DAOD_PHYS.e7620_a899_r13145_p5631
	mc20_13TeV.412116.aMcAtNloHerwig7EvtGen_MEN30NLO_ttbar_SingleLep.deriv.DAOD_PHYS.e7620_a899_r13167_p5631

	mc20_13TeV.412117.aMcAtNloHerwig7EvtGen_MEN30NLO_ttbar_dil.deriv.DAOD_PHYS.e7620_a899_r13144_p5631
	mc20_13TeV.412117.aMcAtNloHerwig7EvtGen_MEN30NLO_ttbar_dil.deriv.DAOD_PHYS.e7620_a899_r13145_p5631
	mc20_13TeV.412117.aMcAtNloHerwig7EvtGen_MEN30NLO_ttbar_dil.deriv.DAOD_PHYS.e7620_a899_r13167_p5631
	```

- Sherpa 2.2.1 (`"410250"`)
    ```
	mc20_13TeV.410250.Sherpa_221_NNPDF30NNLO_ttbar_SingleLeptonP_MEPS_NLO.deriv.DAOD_PHYS.e5450_s3681_r13167_r13146_p5631
	mc20_13TeV.410250.Sherpa_221_NNPDF30NNLO_ttbar_SingleLeptonP_MEPS_NLO.deriv.DAOD_PHYS.e5450_s3681_r13144_r13146_p5631
	mc20_13TeV.410250.Sherpa_221_NNPDF30NNLO_ttbar_SingleLeptonP_MEPS_NLO.deriv.DAOD_PHYS.e5450_s3681_r13145_r13146_p5631

	mc20_13TeV.410251.Sherpa_221_NNPDF30NNLO_ttbar_SingleLeptonM_MEPS_NLO.deriv.DAOD_PHYS.e5450_s3681_r13167_r13146_p5631
	mc20_13TeV.410251.Sherpa_221_NNPDF30NNLO_ttbar_SingleLeptonM_MEPS_NLO.deriv.DAOD_PHYS.e5450_s3681_r13144_r13146_p5631
	mc20_13TeV.410251.Sherpa_221_NNPDF30NNLO_ttbar_SingleLeptonM_MEPS_NLO.deriv.DAOD_PHYS.e5450_s3681_r13145_r13146_p5631

	mc20_13TeV.410252.Sherpa_221_NNPDF30NNLO_ttbar_dilepton_MEPS_NLO.deriv.DAOD_PHYS.e5450_s3681_r13167_r13146_p5631
	mc20_13TeV.410252.Sherpa_221_NNPDF30NNLO_ttbar_dilepton_MEPS_NLO.deriv.DAOD_PHYS.e5450_s3681_r13144_r13146_p5631
	mc20_13TeV.410252.Sherpa_221_NNPDF30NNLO_ttbar_dilepton_MEPS_NLO.deriv.DAOD_PHYS.e5450_s3681_r13145_r13146_p5631
	```

- Powheg + Herwig 7.2.1 + EvtGen (`"600666"`)
    ```
	mc20_13TeV.600666.PhH7EG_H7UE_tt_hdamp258p75_721_singlelep.deriv.DAOD_PHYS.e8312_s3681_r13167_r13146_p5631
	mc20_13TeV.600666.PhH7EG_H7UE_tt_hdamp258p75_721_singlelep.deriv.DAOD_PHYS.e8312_s3681_r13144_r13146_p5631
	mc20_13TeV.600666.PhH7EG_H7UE_tt_hdamp258p75_721_singlelep.deriv.DAOD_PHYS.e8312_s3681_r13145_r13146_p5631

	mc20_13TeV.600667.PhH7EG_H7UE_tt_hdamp258p75_721_dil.deriv.DAOD_PHYS.e8312_s3681_r13167_r13146_p5631
	mc20_13TeV.600667.PhH7EG_H7UE_tt_hdamp258p75_721_dil.deriv.DAOD_PHYS.e8312_s3681_r13144_r13146_p5631
    mc20_13TeV.600667.PhH7EG_H7UE_tt_hdamp258p75_721_dil.deriv.DAOD_PHYS.e8312_s3681_r13145_r13146_p5631
	```

- Sherpa 2.2.12 (`"700660"`)
	```
	mc20_13TeV.700660.Sh_2212_ttbar_dilepton_maxHTavrgTopPT.deriv.DAOD_PHYS.e8461_s3681_r13167_r13146_p5631
	mc20_13TeV.700660.Sh_2212_ttbar_dilepton_maxHTavrgTopPT.deriv.DAOD_PHYS.e8461_s3681_r13144_r13146_p5631
	mc20_13TeV.700660.Sh_2212_ttbar_dilepton_maxHTavrgTopPT.deriv.DAOD_PHYS.e8461_s3681_r13145_r13146_p5631
	
	mc20_13TeV.700661.Sh_2212_ttbar_SingleLeptonM_maxHTavrgTopPT.deriv.DAOD_PHYS.e8461_s3681_r13167_r13146_p5631
	mc20_13TeV.700661.Sh_2212_ttbar_SingleLeptonM_maxHTavrgTopPT.deriv.DAOD_PHYS.e8461_s3681_r13144_r13146_p5631
	mc20_13TeV.700661.Sh_2212_ttbar_SingleLeptonM_maxHTavrgTopPT.deriv.DAOD_PHYS.e8461_s3681_r13145_r13146_p5631
	
	mc20_13TeV.700662.Sh_2212_ttbar_SingleLeptonP_maxHTavrgTopPT.deriv.DAOD_PHYS.e8461_s3681_r13167_r13146_p5631
	mc20_13TeV.700662.Sh_2212_ttbar_SingleLeptonP_maxHTavrgTopPT.deriv.DAOD_PHYS.e8461_s3681_r13144_r13146_p5631
	mc20_13TeV.700662.Sh_2212_ttbar_SingleLeptonP_maxHTavrgTopPT.deriv.DAOD_PHYS.e8461_s3681_r13145_r13146_p5631
	```

No support will be provided for Sherpa 2.2.8. Instead, users are urged towards using samples with a more recent Sherpa version.
No support will be provided for Powheg + Herwig 7.0.4 + EvtGen.

### r21 samples

- Sherpa 2.2.10 (`"700122"`)
	```
	mc16_13TeV.700122.Sh_2210_ttbar_SingleLeptonP_maxHTavrgTopPT_SSC.deriv.DAOD_FTAG1.e8253_e7400_s3126_r9364_r9315_p4380
	mc16_13TeV.700123.Sh_2210_ttbar_SingleLeptonM_maxHTavrgTopPT_SSC.deriv.DAOD_FTAG1.e8253_e7400_s3126_r9364_r9315_p4380
	mc16_13TeV.700124.Sh_2210_ttbar_dilepton_maxHTavrgTopPT_SSC.deriv.DAOD_FTAG1.e8253_e7400_s3126_r9364_r9315_p4380
	mc16_13TeV.700122.Sh_2210_ttbar_SingleLeptonP_maxHTavrgTopPT_SSC.deriv.DAOD_FTAG1.e8253_e7400_s3126_r10201_r10210_p4380
	mc16_13TeV.700123.Sh_2210_ttbar_SingleLeptonM_maxHTavrgTopPT_SSC.deriv.DAOD_FTAG1.e8253_e7400_s3126_r10201_r10210_p4380
	mc16_13TeV.700124.Sh_2210_ttbar_dilepton_maxHTavrgTopPT_SSC.deriv.DAOD_FTAG1.e8253_e7400_s3126_r10201_r10210_p4380
	mc16_13TeV.700122.Sh_2210_ttbar_SingleLeptonP_maxHTavrgTopPT_SSC.deriv.DAOD_FTAG1.e8253_e7400_s3126_r10724_r10726_p4380
	mc16_13TeV.700123.Sh_2210_ttbar_SingleLeptonM_maxHTavrgTopPT_SSC.deriv.DAOD_FTAG1.e8253_e7400_s3126_r10724_r10726_p4380
	mc16_13TeV.700124.Sh_2210_ttbar_dilepton_maxHTavrgTopPT_SSC.deriv.DAOD_FTAG1.e8253_e7400_s3126_r10724_r10726_p4380
	```

- Powheg + Herwig7.2.1 + EvtGen (`"600666"`)
	```
	mc16_13TeV.600666.PhH7EG_H7UE_tt_hdamp258p75_721_singlelep.deriv.DAOD_FTAG1.e8312_e7400_s3126_r9364_r9315_p4418
	mc16_13TeV.600667.PhH7EG_H7UE_tt_hdamp258p75_721_dil.deriv.DAOD_FTAG1.e8312_e7400_s3126_r9364_r9315_p4418
	mc16_13TeV.600666.PhH7EG_H7UE_tt_hdamp258p75_721_singlelep.deriv.DAOD_FTAG1.e8312_e7400_s3126_r10201_r10210_p4418
	mc16_13TeV.600667.PhH7EG_H7UE_tt_hdamp258p75_721_dil.deriv.DAOD_FTAG1.e8312_e7400_s3126_r10201_r10210_p4418
	mc16_13TeV.600666.PhH7EG_H7UE_tt_hdamp258p75_721_singlelep.deriv.DAOD_FTAG1.e8312_e7400_s3126_r10724_r10726_p4418
	mc16_13TeV.600667.PhH7EG_H7UE_tt_hdamp258p75_721_dil.deriv.DAOD_FTAG1.e8312_e7400_s3126_r10724_r10726_p4418
	```

- Sherpa 2.2.8 (`"421152"`): not available anymore, have been obsoleted

- amc@NLO + Pythia8 + EvtGen (`"410464"`)
	```
	mc16_13TeV.410464.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_SingleLep.deriv.DAOD_FTAG1.e6762_a875_r9364_p4062
	mc16_13TeV.410464.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_SingleLep.deriv.DAOD_FTAG1.e6762_a875_r10201_p4062
	mc16_13TeV.410464.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_SingleLep.deriv.DAOD_FTAG1.e6762_a875_r10724_p4062

	mc16_13TeV.410465.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_dil.deriv.DAOD_FTAG1.e6762_a875_r9364_p4062
	mc16_13TeV.410465.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_dil.deriv.DAOD_FTAG1.e6762_a875_r10201_p4062
	mc16_13TeV.410465.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_dil.deriv.DAOD_FTAG1.e6762_a875_r10724_p4062
	```

- Powheg + Herwig 7.1.3 + EvtGen (`"411233"`)
	```
	mc16_13TeV.411233.PowhegHerwig7EvtGen_tt_hdamp258p75_713_SingleLep.deriv.DAOD_FTAG1.e7580_a875_r9364_p4062
	mc16_13TeV.411233.PowhegHerwig7EvtGen_tt_hdamp258p75_713_SingleLep.deriv.DAOD_FTAG1.e7580_a875_r10201_p4062
	mc16_13TeV.411233.PowhegHerwig7EvtGen_tt_hdamp258p75_713_SingleLep.deriv.DAOD_FTAG1.e7580_a875_r10724_p4062

	mc16_13TeV.411234.PowhegHerwig7EvtGen_tt_hdamp258p75_713_dil.deriv.DAOD_FTAG1.e7580_a875_r9364_p4062
	mc16_13TeV.411234.PowhegHerwig7EvtGen_tt_hdamp258p75_713_dil.deriv.DAOD_FTAG1.e7580_a875_r10201_p4062
	mc16_13TeV.411234.PowhegHerwig7EvtGen_tt_hdamp258p75_713_dil.deriv.DAOD_FTAG1.e7580_a875_r10724_p4062	
	```

- Powheg + Herwig 7.0.4 + EvtGen (`"410558"`)
	```
	mc16_13TeV.410557.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_704_SingleLep.deriv.DAOD_FTAG1.e6366_a875_r9364_p4062
	mc16_13TeV.410557.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_704_SingleLep.deriv.DAOD_FTAG1.e6366_a875_r10201_p4062
	mc16_13TeV.410557.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_704_SingleLep.deriv.DAOD_FTAG1.e6366_a875_r10724_p4062

	mc16_13TeV.410558.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_704_dil.deriv.DAOD_FTAG1.e6366_a875_r9364_p4062
	mc16_13TeV.410558.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_704_dil.deriv.DAOD_FTAG1.e6366_a875_r10201_p4062
	mc16_13TeV.410558.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_704_dil.deriv.DAOD_FTAG1.e6366_a875_r10724_p4062
	```

- Sherpa 2.2.1 (`"410250"`)
	```
	mc16_13TeV.410250.Sherpa_221_NNPDF30NNLO_ttbar_SingleLeptonP_MEPS_NLO.deriv.DAOD_FTAG1.e5450_s3126_r9364_p4062
	mc16_13TeV.410250.Sherpa_221_NNPDF30NNLO_ttbar_SingleLeptonP_MEPS_NLO.deriv.DAOD_FTAG1.e5450_s3126_r10201_p4062

	mc16_13TeV.410251.Sherpa_221_NNPDF30NNLO_ttbar_SingleLeptonM_MEPS_NLO.deriv.DAOD_FTAG1.e5450_s3126_r9364_p4062
	mc16_13TeV.410251.Sherpa_221_NNPDF30NNLO_ttbar_SingleLeptonM_MEPS_NLO.deriv.DAOD_FTAG1.e5450_s3126_r10201_p4062

	mc16_13TeV.410252.Sherpa_221_NNPDF30NNLO_ttbar_dilepton_MEPS_NLO.deriv.DAOD_FTAG1.e5450_s3126_r9364_p4062
	mc16_13TeV.410252.Sherpa_221_NNPDF30NNLO_ttbar_dilepton_MEPS_NLO.deriv.DAOD_FTAG1.e5450_s3126_r10201_p4062
	```
	
	For Sherpa 2.2.1 there is no ttbar MC16e sample available, and the effect for the maps including them was found to be negligible. Thus the maps are only evaluated using MC16a and MC16d

