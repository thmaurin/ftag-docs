# Impact parameter based low-level algorithms

Documentation for low-level taggers:

- [IPTag](https://gitlab.cern.ch/fdibello/IPtagTuning)
- [RNNIP](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingRNNIP)
- [DIPS](https://ftag.docs.cern.ch/algorithms/dips/)


## IPTag

The IPTag family of taggers uses the impact parameters of tracks to identify tracks associated with B-mesons. There are two taggers, the IP2D and IP3D low-level algorithms.
The IP2D tagger makes use of the transverse impact parameter significance $d_0 / \sigma d_0$ as discriminating variable, whereas IP3D uses both the transverse and the longitudinal impact parameter significance $z_0 \sin \theta / \sigma z_0 \sin \theta$ in a two-dimensional template to account for their correlation.

The IP2D and IP3D algorithms are based on a log-likelihood-ratio (LLR) discriminant. This discriminant is computed as the sum of per-track contributions
$$
\sum_i^N \log \left(\frac{p_b}{p_u}\right),
$$
where $N$ is the number of tracks for a given jet and $p_b$, $p_u$ are the template probability density functions (PDF) for the b- and light-flavour jet flavour hypotheses, respectively, assuming no cor- relation among the various tracks contributing to the sum.
Similar LRR quantities can be defined for ratios of b-jets to c-jets and ratios of c-jets to light-flavour jets.

These template probability density functions are obtained from reference histograms for the transverse and longitudinal impact parameter significances and are derived from MC simulation. They are separated into exclusive categories (the track grade) that depend on the hit pattern of a given track to increase the discriminating power, and used to calculate ratios of the jet jet flavour probabilities. This computation is performed on a per-track basis.

## RNNIP

## DIPS

See the article about DIPS here: [DIPS algorithm documentation](https://ftag.docs.cern.ch/algorithms/dips)