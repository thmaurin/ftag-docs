## Training Samples

When performing studies for flavour tagging, please use the H5 samples listed [here](../../samples/samples.md) to avoid using superseeded recommendations.
This is in particular the case for release 22 samples, where lots of changes are happening currently.

Please keep always track of which exact sample you were using for your studies.


## Training software

Algorithms are trained with software tools documented [here](../../software/index.md).
