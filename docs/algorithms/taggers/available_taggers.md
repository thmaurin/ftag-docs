# FTAG - Latest Taggers

Overview to keep track of the current available and latest taggers

For all taggers the following base paths apply:

- cvmfs: `/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/`
- EOS: `/eos/atlas/atlascerngroupdisk/asg-calib/`
- website: https://atlas-groupdata.web.cern.ch/atlas-groupdata/

Besides the lwtnn models in the group area, also the keras models together with the scale dictionaries as well as the variable dictionaries are saved in an eos are `/eos/user/u/umamibot/ftag-models` (everyone on the e-group `atlas-cp-flavtag-unami-developers` has write access).

- Information on how to schedule the networks in the training-dataset-dumper is provided [here](https://training-dataset-dumper.docs.cern.ch/configuration/#dl2-config).
- The procedure of adding new networks to the group area is described [here](https://ftag.docs.cern.ch/software/grouparea/).

### Short explanations

Track selections:

- `R22 default track selection`: indicates the default r22 track selection used for the tagger trainings which use tracks (e.g. DIPS, GNN, RNNIP). The cuts are defined e.g. [here](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/main/configs/fragments/r22default-track-cuts.json).
- `R22 loose track selection`: indicates the loose r22 track selection used for the tagger trainings which use tracks (e.g. DIPS, GNN, RNNIP). The cuts are defined e.g. [here](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/main/configs/fragments/r22loose-track-cuts.json).
- `IP3D track selection`: indicates the IP3D-like track selection used for the tagger trainings which use tracks (e.g. DIPS, GNN, RNNIP). The cuts are defined e.g. [here](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/main/configs/fragments/ip3d-track-cuts.json).
- `Loose track selection` (current default, will be superseded by `R22 default track selection`): indicates the loose track selection used for the tagger trainings which use tracks (e.g. DIPS, GNN, RNNIP). The cuts are defined e.g. [here](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/main/configs/fragments/ip3d-loose-track-cuts.json).
- `GNN loose selection`: indicates the even looser track selection used for GNN training studies. The cuts are defined e.g. [here](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/main/configs/fragments/gnn-track-cuts.json).


Details concerning the samples (changelog) for the p-tags used in the trainings can be found [here](https://ftag.docs.cern.ch/algorithms/overview/).

## R22

### Round 3 models

#### trained on r22 p4931 or p5169 with 20M jets or more

???+ info "GNN [trained on r22 p5169 with 192M jets]"

    - [Graph Neural Network (GNN)](https://ftag.docs.cern.ch/algorithms/GNN) tagger trained on r22 p5169 with 192M jets (using PDF sampling), approx 72M/72M/49M jets each for b-, c- and light-flavour jets
    - using GN2 architecture (transformer-based)
    - trained with *r22default* track selection
        - Location in group area: `BTagging/20230306/gn2v00/antikt4empflow/network.onnx`
        - Location in development group area: `dev/BTagging/20230306/gn2v00/antikt4empflow/network.onnx`
        - Tagger name: `GN2v00`
        - Location on EOS: `/eos/user/u/umamibot/ftag-models/r22/GN2v00/20230306/antikt4empflow`

???+ info "GNN for VR track jets [trained on r22 p5169 with 20M jets]"

    - [Graph Neural Network (GNN)](https://ftag.docs.cern.ch/algorithms/GNN) tagger trained on r22 p5169 with 19.5M jets (using count sampling), considering ttbar and zprime samples
    - using GN2 architecture (transformer-based)
    - *r22default* track selection:
        - Location in group area: `BTagging/20230307/gn2v00/antiktvr30rmax4rmin02track/network.onnx`
        - Location in development group area: `dev/BTagging/20230307/gn2v00/antiktvr30rmax4rmin02track/network.onnx`
        - Tagger name: `GN2v00`
        - Location on EOS: `/eos/user/u/umamibot/ftag-models/r22/GN2v00/20230307/antiktvr30rmax4rmin02track`

???+ info "DL1d for VR track jets [trained on r22 p5169 with 120M jets]"

    - [Deep Learning 1 with DIPS input (DL1d)](https://ftag.docs.cern.ch/algorithms/dips) tagger trained on trained on r22 p5169 with 120M jets (using PDF sampling), 40M jets each for b-, c- and light-flavour jets
    - Loose track selection of DIPS (with at least 7 silicon hits):
        - Location in group area: `BTagging/20230307/DL1dv01/antiktvr30rmax4rmin02track/network.json`
        - Location in development group area: `dev/BTagging/20230307/DL1dv01/antiktvr30rmax4rmin02track/network.json`
        - Tagger name: `DL1dv01`
        - Location on EOS: `/eos/user/u/umamibot/ftag-models/r22/DL1dLoose/20230307/antiktvr30rmax4rmin02track`

???+ info "DIPS for VR track jets [trained on r22 p5169 with 120M jets]"

    - [Deep Impact Parameter Sets (DIPS)](https://ftag.docs.cern.ch/algorithms/dips) tagger trained on r22 p5169 with 120M jets (using PDF sampling), 40M jets each for b-, c- and light-flavour jets
    - Loose track selection (with at least 7 silicon hits):
        - Location in group area: `BTagging/20230208/dipsLoose/antiktvr30rmax4rmin02track/network.json`
        - Location in development group area: `dev/BTagging/20230208/dipsLoose/antiktvr30rmax4rmin02track/network.json`
        - Tagger name: `dipsLooseVR20230208`
        - Location on EOS: `/eos/user/u/umamibot/ftag-models/r22/dipsLoose/20230208/antiktvr30rmax4rmin02track`

??? info "DIPS [trained on r22 p4931 with 120M jets]"

    - [Deep Impact Parameter Sets (DIPS)](https://ftag.docs.cern.ch/algorithms/dips) tagger trained on r22 p4931 with 120M jets (using PDF sampling), 40M jets each for b-, c- and light-flavour jets, c-jets are upsampled by a factor of 1.7.
    - Loose track selection:
        - Location in development group area: `dev/BTagging/20221102/dipsLoose/antikt4empflow/network.json`
        - Tagger name: `dipsLoose20221102`
        - Location on EOS: `/eos/user/u/umamibot/ftag-models/r22/dipsLoose/20221102/antikt4empflow`


??? info "DIPS [trained on r22 p4931 with 135M jets] with four classes (including taus)"

    - [Deep Impact Parameter Sets (DIPS)](https://ftag.docs.cern.ch/algorithms/dips) tagger trained on r22 p4931 with 135M jets (using PDF sampling), 40M jets each for b-, c- and light-flavour jets and 15M tau-jets, c-jets are upsampled by a factor of 1.7. Extended 4-classes training with b-, c- and light-flavour- and tau-jets.
    - Loose track selection:
        - Location in development group area: `dev/BTagging/20221102/dipsLooseTaus/antikt4empflow/network.json`
        - Tagger name: `dipsLooseTaus20221102`
        - Location on EOS: `/eos/user/u/umamibot/ftag-models/r22/dipsLooseTaus/20221102/antikt4empflow`


### Round 2 models

#### trained on r22 p4931 with 23M jets

???+ info "DL1d [trained on r22 p4931 with 23M jets]"

    - [Deep Learning 1 with DIPS input (DL1d)](https://ftag.docs.cern.ch/algorithms/dips) tagger trained on r22 p4931 with 23M jets
    - Loose track selection of DIPS:
        - Location in group area: `BTagging/20220509/dl1dLoose/antikt4empflow/network.json`
        - Tagger name: `DL1dv01`
        - Dips input: `dipsLoose20220314v2`
        - Location on EOS: `/eos/user/u/umamibot/ftag-models/r22/DL1dLoose/20220509/antikt4empflow/`


??? info "GNN [trained on r22 p4931 with 23M jets]"

    - [Graph Neural Network (GNN)](https://ftag.docs.cern.ch/algorithms/GNN) tagger trained on r22 p4931 with 23M jets
    - trained with *loose track selection*
        - Location in group area: `BTagging/20220509/gn1/antikt4empflow/network.onnx`
        - Tagger name: `GN120220509`
        - Location on EOS: `/eos/user/u/umamibot/ftag-models/r22/GN1/20220509/antikt4empflow`
    - trained with *loose track selection* and `leptonID` as track feature
        - Location in group area: `BTagging/20220509/gn1lep/antikt4empflow/network.onnx`
        - Tagger name: `GN1Lep20220509`
        - Location on EOS: `/eos/user/u/umamibot/ftag-models/r22/GN1lep/20220509/antikt4empflow`


??? bug "GNN [trained on r22 p4931 with 23M jets]"

    - [Graph Neural Network (GNN)](https://ftag.docs.cern.ch/algorithms/GNN) tagger trained on r22 p4931 with 23M jets
    - **!!!BROKEN!!! Don't use this one!!!**
    - trained with *loose track selection*
        - Location in development group area: `dev/BTagging/20220420/gn1/antikt4empflow/network.onnx`
        - Tagger name: `GN120220422`
        - Location on EOS: `/eos/user/u/umamibot/ftag-models/r22/GN1/20220420/antikt4empflow`


??? info "CADS [trained on r22 p4931 with 23M jets]"

    - [Conditional Attention Deep Impact Parameter Sets (CADS)](https://ftag.docs.cern.ch/algorithms/dips) tagger trained on r22 p4931 with 23M jets
    - No onnx model or support for the dumper yet!
    - Loose track selection:
        - Location in group area: `none`
        - Tagger name: `none`
        - Location on EOS: `/eos/user/u/umamibot/ftag-models/r22/CADS/20220314/antikt4empflow/`


???+ info "DIPS [trained on r22 p4931 with 23M jets]"

    - [Deep Impact Parameter Sets (DIPS)](https://ftag.docs.cern.ch/algorithms/dips) tagger trained on r22 p4931 with 23M jets
    - Loose track selection:
        - Location in group area: `BTagging/20220314/dipsLoose/antikt4empflow/network.json`
        - Location in development group area: `dev/BTagging/20220314v2/dipsLoose/antikt4empflow/network.json`
        - Tagger name: `dipsLoose20220314v2`
        - Location on EOS: `/eos/user/u/umamibot/ftag-models/r22/dipsLoose/20220314v2/antikt4empflow`


??? bug "DIPS [trained on r22 p4931 with 23M jets]"

    - [Deep Impact Parameter Sets (DIPS)](https://ftag.docs.cern.ch/algorithms/dips) tagger trained on r22 p4931 with 23M jets
    - **!!!BROKEN!!! Don't use this one!!!**:
    - Loose track selection:
        - Location in development group area: `dev/BTagging/20220314/dipsLoose/antikt4empflow/network.json`
        - Tagger name: `dipsLoose20220314`
        - Location on EOS: `/eos/user/u/umamibot/ftag-models/r22/dipsLoose/20220314/antikt4empflow/`


#### trained on r22 p4567

??? info "DIPS [trained on r22 p4567]"

    - [Deep Impact Parameter Sets (DIPS)](https://ftag.docs.cern.ch/algorithms/dips) tagger trained on r22 p4567
    - Loose track selection:
        - Location in group area: `BTagging/20210729/dipsLoose/antikt4empflow/network.json`
        - Tagger name: `dipsLoose20210729`
        - Location on EOS: `/eos/user/u/umamibot/ftag-models/r22/dipsLoose/20210729/antikt4empflow/`
    - IP3D track selection:
        - Location in group area: `BTagging/20210729/dips/antikt4empflow/network.json`
        - Tagger name: `dips20210729`
        - Location on EOS: `/eos/user/u/umamibot/ftag-models/r22/dips/20210729/antikt4empflow/`


??? info "DL1d [trained on r22 - p4567]"

    - [Deep Learning 1 with DIPS input (DL1d)](https://ftag.docs.cern.ch/algorithms/dips) tagger trained on r22 p4567
    - Loose track selection of DIPS:
        - Location in development group area: `dev/BTagging/20210824r22/dl1dLoose/antikt4empflow/network.json`
        - Tagger name: `DL1dLoose20210824r22`
        - Dips input: `dipsLoose20210729`
    - Loose track selection of DIPS: **RENAMED as `DL1dv00` to be used as default**
        - This tagger is the same as `DL1dLoose20210824r22` but has been renamed to `DL1dv00` for deployment to the group area.
        - Location in group area: `BTagging/20210824r22/dl1dLoose/antikt4empflow/network.json`
        - Tagger name: `DL1dv00`
        - Dips input: `dipsLoose20210729`
    - IP3D track selection of DIPS:
        - Location in development group area: `dev/BTagging/20210824r22/dl1d/antikt4empflow/network.json`
        - Tagger name: `DL1d20210824r22`
        - Dips input: `dips20210729`


??? info "DL1r -  [trained on r22 - p4567]"

    - [Deep Learning 1 with RNNIP input (DL1r)](https://ftag.docs.cern.ch/algorithms/dips) tagger trained on r22 p4567
    - IP3D track selection of RNNIP:
        - Location in development group area: `dev/BTagging/20210824r22/dl1r/antikt4empflow/network.json`
        - Tagger name: `DL1r20210824r22`


### Round 1 models

#### trained on r22 p4505

??? info "DL1d - loose DIPS [trained on r22 - p4505]"

    - [Deep Learning 1 with DIPS input (DL1d)](https://ftag.docs.cern.ch/algorithms/dips) tagger trained on r22 p4567
    - Loose track selection:
        - Location in group area: `BTagging/20210519r22/dl1d/antikt4empflow/network.json`
        - Tagger name: `DL1d20210519r22`
    - Loose track selection with taus:
        - Location in group area: `BTagging/20210519r22/dl1dtaus/antikt4empflow/network.json`
        - Tagger name: `DL1d20210519r22taus`

??? info "DL1d - default DIPS [trained on r22 - p4505]"

    - [Deep Learning 1 with DIPS input (DL1d)](https://ftag.docs.cern.ch/algorithms/dips) tagger trained on r22 p4567
    - IP3D track selection:
        - Location in group area: `BTagging/20210528r22/dl1d/antikt4empflow/network.json`
        - Tagger name: `DL1d20210528r22`

??? info "DL1r [trained on r22 - p4505]"

    - [Deep Learning 1 with RNNIP input (DL1r)](https://ftag.docs.cern.ch/algorithms/dips) tagger trained on r22 p4567
    - IP3D track selection:
        - Location in group area: `BTagging/20210519r22/dl1r/antikt4empflow/network.json`
        - Tagger name: `DL1r20210519r22`
    - IP3D track selection with taus:
        - Location in group area: `BTagging/20210519r22/dl1rtaus/antikt4empflow/network.json`
        - Tagger name: `DL1r20210519r22taus`

#### trained on r21

??? info "DIPS [trained on r21]"

    - [Deep Impact Parameter Sets (DIPS)](https://ftag.docs.cern.ch/algorithms/dips) tagger trained on r21
    - Loose track selection:
        - Location in group area: `BTagging/20210517/dipsLoose/antikt4empflow/network.json`
        - Tagger name: `dipsLoose20210517`
    - IP3D track selection:
        - Location in group area: `BTagging/20210517/dips/antikt4empflow/network.json`
        - Tagger name: `dips20210517`

??? bug "Umami [trained on r21]"

    - [UMAMI](https://ftag.docs.cern.ch/algorithms/umami/) tagger trained on r21
    - **!!!BROKEN!!! Don't use this one!!!**:
    - Loose track selection:
        - Location in development group area: `dev/BTagging/20210506r22/umami/antikt4empflow/network.json`
        - Tagger name: `UMAMI20210506r22` and `dips_UMAMI20210506r22`

## Track-based taggers

???+ info "NNTC [trained on r22]"

    - [Neural Network Track Classification (NNTC)](https://ftag.docs.cern.ch/algorithms/dips) tagger trained on r22
    - [GNN loose selection](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/r22/configs/single-b-tag/fragments/gnn-track-cuts.json):
        - Location in group area: `dev/BTagging/20220519/nntc/network.json`
        - Tagger name: `nntc20220519`
        - location on `eos`: `/eos/user/u/umamibot/ftag-models/r22/nntc/20220519/`


## Upgrade (RUN4)

???+ info "GN1 [trained on 4.02M jets]"

    - [Graph Neural Network (GNN)](https://ftag.docs.cern.ch/algorithms/GNN) tagger trained on upgrade samples with 4.02M jets
    - Loose track selection of DIPS:
        - Location in group area (use this): `BTagging/20221010/GN1run4/antikt4emtopo/network.onnx`
        - Location in development group area (for reference): `dev/BTagging/20221010/GN1run4/antikt4emtopo/network.onnx`
        - Tagger name: `GN1run420221010`
        - Location on EOS: `/eos/user/u/umamibot/ftag-models/upgrade/gn1run4/20221010/antikt4emtopo`

???+ info "DL1d [trained on 15M jets]"

    - [Deep Learning 1 with DIPS input (DL1d)](https://ftag.docs.cern.ch/algorithms/dips) tagger trained on upgrade samples with 15M jets
    - Loose track selection of DIPS:
        - Location in group area (use this): `BTagging/20221017/dl1drun4/antikt4emtopo/network.json`
        - Location in development group area (for reference): `dev/BTagging/20221017/dl1drun4/antikt4emtopo/network.json`
        - Tagger name: `DL1drun420221017`
        - Dips input: `dipsrun420221008`
        - Location on EOS: `/eos/user/u/umamibot/ftag-models/upgrade/dl1drun4/20221017/antikt4emtopo`


???+ info "Dips [trained on 15M jets]"

    - [DIPS](https://ftag.docs.cern.ch/algorithms/dips) tagger trained on upgrade samples with 15M jets
    - Loose track selection of DIPS:
        - Location in group area (use this): `BTagging/20221008/dipsrun4/antikt4emtopo/network.json`
        - Location in development group area (for reference): `dev/BTagging/20221008/dipsrun4/antikt4emtopo/network.json`
        - Tagger name: `dipsrun420221008`
        - Location on EOS: `/eos/user/u/umamibot/ftag-models/upgrade/dipsrun4/20221008/antikt4emtopo`


??? info "DL1d [trained on 3.2M jets]"

    - [Deep Learning 1 with DIPS input (DL1d)](https://ftag.docs.cern.ch/algorithms/dips) tagger trained on upgrade samples with 3.2M jets
    - IP3D track selection of DIPS:
        - Location in group area: `dev/BTagging/20220930/dl1drun4/antikt4emtopo/network.json`
        - Tagger name: `DL1drun420220930`
        - Dips input: `dipsrun420220707`
        - Location on EOS: `/eos/user/u/umamibot/ftag-models/upgrade/dl1drun4/20220930/antikt4emtopo`


??? info "Dips [trained on XXM jets]"

    - [DIPS](https://ftag.docs.cern.ch/algorithms/dips) tagger trained on upgrade samples with XXM jets
    - IP3D track selection of DIPS:
        - Location in group area: `dev/BTagging/20220707/dipsrun4/antikt4emtopo/network.json`
        - Tagger name: `dipsrun420220707`
        - Location on EOS: `/eos/user/u/umamibot/ftag-models/upgrade/dipsrun4/20220707/antikt4emtopo`


## Trigger level taggers

### Round 1 models

#### trained on r22 d1677_r12711

??? info "DIPS [trained on r22 d1677_r12711]"

    - Loose track selection:
        - Location in development group area: `dev/BTagging/20211216trig/dips/AntiKt4EMPFlow/network.json`
        - Tagger name: `dips20211116`
        - location on eos: `/eos/atlas/atlascerngroupdisk/trig-bjet/trigger-tunning/models/dipsLoose-model-2021-11-16.json`
    - IP3D track selection:
        - Location in development group area: `dev/BTagging/20211116trig/dips/AntiKt4EMPFlow/network.json`
        - Tagger name: `dips20211116`
        - location on eos: `/eos/atlas/atlascerngroupdisk/trig-bjet/trigger-tunning/models/dips-model-2021-11-16.json`

??? info "DL1d - loose DIPS [trained on r22 d1677_r12711]"

    - Loose track selection of DIPS:
        - Location in development group area: `dev/BTagging/20211216trig/dl1d/AntiKt4EMPFlow/network.json`
        - Tagger name: `DL1d20211216`
        - location on eos: `/eos/atlas/atlascerngroupdisk/trig-bjet/trigger-tunning/models/dl1d-2021-12-16.json`

#### trained on r21

<!-- ## R21  -->
??? note "R21"
    - DIPS [trained on r21]:
        - Loose track selection: `BTagging/20210517/dipsLoose/antikt4empflow/network.json`
            - Tagger name: `dipsLoose20210517`
            - location on eos: `/eos/user/u/umamibot/ftag-models/r21/dipsLoose/20210517/antikt4empflow/`
        - IP3D track selection: `BTagging/20210517/dips/antikt4empflow/network.json`
            - Tagger name: `dips20210517`
            - location on eos: `/eos/user/u/umamibot/ftag-models/r21/dips/20210517/antikt4empflow/`

    - DL1d [trained on r21]:
        - IP3D track selection of DIPS input: `BTagging/20210607/dl1d/antikt4empflow/network.json`
            - Tagger name: `DL1d20210607`
            - location on eos: `/eos/user/u/umamibot/ftag-models/r21/DL1d/20210607/antikt4empflow`
        -  Loose track selection of DIPS input: `BTagging/20210607/dl1dLoose/antikt4empflow/network.json`
            - Tagger name: `DL1dLoose20210607`
            - location on eos: `/eos/user/u/umamibot/ftag-models/r21/DL1dLoose/20210607/antikt4empflow`
    - Umami [trained on r21]:
        -  Loose track selection: `dev/BTagging/20210906/umami/antikt4empflow/network.json`
            - Tagger name: `UMAMI20210906` and `dips_UMAMI20210906`
            - location on eos: `/eos/user/u/umamibot/ftag-models/r21/umami/20210906/antikt4empflow/`
    - DIPS-bb [trained on r21]:

        This model is an extended version of DIPS, adding a *bb*-class.
        This means that the output is `(p_u, p_c, p_b, p_bb)`.
        The remaining network architecture is the same as for DIPS and the same input variables are used.
        For further details have a look at the slides [here](https://indico.cern.ch/event/1123953/contributions/4743517/attachments/2393083/4091311/bb_classification_with_DIPS_Joschka_Birk.pdf)

        - IP3D track selection of DIPS input:
            - Tagger name: `dipsbb20220223`
            - location on eos: `/eos/user/u/umamibot/ftag-models/r21/dipsbb/20220223/antikt4empflow/`
            - location in development group area: `dev/BTagging/20220223/dipsbb/antikt4empflow/network.json`
        - Loose track selection of DIPS input:
            - Tagger name: `dipsbbLoose20220407`
            - location on eos: `/eos/user/u/umamibot/ftag-models/r21/dipsbbLoose/20220407/antikt4empflow`
            - location in group area: `dev/BTagging/20220407/dipsbbLoose/antikt4empflow/network.json`
