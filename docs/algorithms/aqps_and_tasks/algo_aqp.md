[jira]: https://its.cern.ch/jira/secure/Dashboard.jspa?selectPageId=22707
[AFT]: https://its.cern.ch/jira/projects/AFT

## Introduction

The authorship qualification projects in the algorithms subgroup are maintained in
[JIRA][AFT].

You can view all ongoing and recently completed algorithms AQPs using [this dashboard][jira].

If an AQP is missing, please ensure that the `QualificationTask` and `Algorithms` tags are added to the `Component/s` field.

## Open Tasks

You can also find suggestions for authorship projects on [the next page][open-tasksaqps].
