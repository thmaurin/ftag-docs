ATLAS has compiled a number of tutorials to help new members get started. See for example:

- [The list of ATLAS tutorials on indico][ti]
- [A corporate-level overview TWiki from management][man]

But with almost 3000 active members with over a hundred funding sources, there's almost nothing that everyone can agree on! As such these overviews (while very informational) can be a bit watered down.

The ATLAS Flavor Tagging group can do better.
Here we list of what we expect of you if you are working in the group.

!!! info "If you are starting an AQP, you should also read the [specific instructions for AQPs](aqp.md)."

### General

You will do _everything_ more than once, and _nothing_ is ever finished.
Don't ever assume this is the last hack, the last ntuple production, the last CDI, etc.
We'll _always_ need another one. Plan your work accordingly.

### Mattermost

We use mattermost for day-to-day communication.

- You can sign up with [the links on the main page](../index.md).
- Ask questions and complain. Don't worry about making noise[^all], we'll make designated channels as needed.
- **Avoid private channels!** Even if you are chatting with _the_ expert there are probably others who can help. It benefits everyone the group when less-known experts step up and help you debug.

[^all]: There's a big difference between posting in a channel and explicitly `@`-mentioning people, as the later implies that you need help from somenoe specific. Please do not use `@all` or `@channel` unless you need _everyone_ in the channel to respond!

??? Tip "Link to posts!"

    Mattermost provides permanent links to every post, which can be useful as a reference when reporting issues.

    ![](../assets/link.png)

    Click the "..." and then the "Copy Link" button

#### Use Threads

Please use threads! This makes conversations much easier to follow. You should also enable enable collapsed reply threads (see below).

??? Info "Enable collapsed reply threads!"

    Channels can get very cluttered if you don't use threads. Unfortunately by default threads spam the main channel too, which can lead to exceptionally incoherent conversations. Fortunately you can change this under "settings":

    ![](../assets/settings.png)

    by enabling "collapsed reply threads:

    ![](../assets/collapsed-reply-threads.png)

    **We strongly encourage everyone to enable** this feature: it will keep you from accidentally breaking the thread and confusing everyone.


### Code & Gitlab

All of your code should be version controlled on GitLab.
If this seems cumbersome you probably aren't doing it right.
You should work in a [fork][fork] for common FTAG software projects.

- **You never need to ask to open a merge request** (MR), remember that it's just a "request".
- Make small MRs! Several small MRs are much easier to review than one big one.
- Early feedback is essential! Start `Draft:` merge requests as soon as possible.
- Use `@`-mentions (e.g. `@dguest`) to tag sub conveners and any other experts.
- Make aesthetic and formatting changes in designated merge requests[^fmt].
- If you aren't sure how to fix something, or want a general change, make an issue in the repository. Feel free to post issues to mattermost.

[^fmt]: ATLAS code formatting is terrible, but merge requests that change both functionality and formatting are much harder to review. If you must change formatting, make these changes in a designated merge request.

#### MR Review

!!! tip "Following this guidance will save you a lot of trouble. MRs that remain open for a long time are likely to pick up messy conflicts."

For work in progress MRs, label them as `Draft:`. Early feedback is essential, especially for bigger changes.

When your merge request is nearing readiness, you need to proactively do a few things to ensure your changes are efficiently incorporated into the main codebase. Follow these steps in order:

1. If your project has CI,[^ci] make sure all tests are passing.
2. **Remove the `Draft:` status:** Most package maintainers will assume that `Draft:` merge requests aren't ready for a final review.[^draft]
3. Alert (`@`-mention) the reviewers.

??? warning "Is your branch out of sync?"
    Check whether your branch is in sync with `main`. If status box underneath the description says `Merge blocked: the source branch must be rebased onto the target branch` then you **need** to [rebase][rebase] or [merge][merge] using `main`.

[^ci]: If your project does _not_ have CI, we are happy to help you add it. **We give OTP for this**, and will buy you beer (or your beverage of choice). As always, please get in contact with conveners early for this kind of major change, there are right and wrong ways to introduce CI.
[^draft]: It's important to remember who the reviewers are in any given repository. This makes Athena somewhat exceptional: the Athena reviewers are _not_ accountable to the flavor tagging group. In the Athena case (and _only_ in the Athena case) you should make sure the flavor tagging group is happy with changes _before_ removing `Draft:`.

[fork]: https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow
[rebase]: https://www.atlassian.com/git/tutorials/rewriting-history/git-rebase
[merge]: https://www.atlassian.com/git/tutorials/using-branches/git-merge

### Presentations & Indico

??? Info "Do two names appear on your talk? Fix it!"

    It's very common for ATLAS members to end up with duplicated Indico accounts. This can be confusing: if we add you under an account you no longer use, you won't be able to upload material.

    Fortunately the Indico team is very quick to fix this. You can [file a ticket with them][dup] which states:

    - that you have (or someone else has) two accounts, and
    - that these should be merged
    - which account should be the "primary" one after merging (i.e. which email and name to keep)

    If you file this for someone else, make sure you add them to the watch list and _ask them to confirm!_

For better or worse, slides in Indico serve the duel purposes of **a prop for presentation** and **a reference for future**. These purposes aren't always aligned and as such it's easy to neglect one of them.

#### Presenting

- When asked to present, **ask how long the talk should be**[^length]. Do _not_ go over the allotted time.
    - Please tell conveners / subconveners (privately if you like) when the conversation goes off subject or the meeting is falling behind schedule. We _do_ respect your time, but it's easy for us to get carried away.
    - Leave time for questions: many agenda haven't factored this in.
- Avoid busy slides:
    - Don't show more than 2 plots per slide. If you need more, you probably aren't making the right plots.
    - Everything should make a point: if it's unremarkable put it in backup slides.
    - Don't show tables. They are fine in backup slides.

[^length]: The indico default assigns 20 minutes per talk, but one will complain if you can make your point in 5 minutes. Conversely, if it will take 40 minutes to cover everything _let us know_, we are happy to accommodate.

#### Slides as a reference

- There are no limits to your backup slides! Put any tables, and plots with many overlaid lines there.
- Use a lot of links:
    - If you borrow a plot, link to the original
    - If you make a plot, link to the code that produced it
- Be proactive: **report technical issues outside your talk!** Make a gitlab issue, merge request, or jira ticket, and include a link in your talk.

[dup]: https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=Indico-Service

#### General

- Number your slides.
- For talks over 10 minutes, **spend 1 minute reviewing your project**. Most people in the meeting will know far less than you, and your purpose is to engage with _everyone_.



### Documentation

ATLAS documentation could be better, but writing good documentation is _hard_ and takes a lot of experience.

We encourage you to improve documentation, and offer a few tips:

- Contribute to existing documentation pages whenever something is unclear to you. If you don't understand something, it's likely that others don't either.
- Give a general overview: what problem does this project solve, and why is this solution better than anything that exists?
- Avoid specifics: these are the first thing to go out of date. This is especially true of code, where variable names or function calls might change, but the general purpose of the code will remain the same.
- If something is hard to explain, it might be a bad idea. Rather than documenting a convoluted measurement or piece of code, consider whether you could simplify it and document the simplified result. If the result is that we have less to maintain we'll thank you!


[ti]: https://indico.cern.ch/category/397/
[man]: https://twiki.cern.ch/twiki/bin/viewauth/Atlas/WelcometoATLAS
[jira]: https://its.cern.ch/jira/secure/Dashboard.jspa?selectPageId=24603
[ac]: https://twiki.cern.ch/twiki/bin/view/Atlas/AuthorShipCommittee
[eg]: https://groups.cern.ch/Pages/GroupSearch.aspx
