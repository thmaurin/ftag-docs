[jira]: https://its.cern.ch/jira/secure/Dashboard.jspa?selectPageId=24603
[AFT]: https://its.cern.ch/jira/projects/AFT

# Qualification Projects (AQP)

A list of Authorship Qualification Projects (also called Qualification Tasks or QTs) in flavor tagging are
maintained in [JIRA][jira].
If an AQP is missing, please ensure that `QualificationTask` is in the `Component/s` field.

There are also dedicated pages for for [algorithms AQPs](../algorithms/aqps_and_tasks/algo_aqp.md) and [calibration AQPs](../calibrations/cal_aqps.md).



## Expectations

!!! info "In addition to the guidance on this page, you should also follow the general points in the [how to guide](how_to_ftag.md)."

Your qualification task is tracked in Jira, see [the Flavor Tagging QT dashboard][jira]. These tickets are an essential record: incoming conveners use them to get up to speed, the [Authorship Committee][ac] and physics coordinators may review them before signing off, and _you_ should use them to keep track of your personal progress.

You should update your Jira ticket:

- Every time you give a talk in a flavor tagging meeting, or
- every 2 weeks,

whichever comes first. These updates should give a short 1 or 2 sentences explaining what you did in the last few weeks.

[ac]: https://twiki.cern.ch/twiki/bin/view/Atlas/AuthorShipCommittee
[jira]: https://its.cern.ch/jira/secure/Dashboard.jspa?selectPageId=24603



## Requesting an AQP

The procedure to initiate a new AQP is summarized in the following flowchart:

```mermaid
flowchart TD

chat("Discussion"):::inst
create_ticket("Create JIRA Ticket\n<b>(Anyone)</b>"):::inst
edit_ticket("Enter final text in JIRA\n<b>(Subgroup Convener)</b>"):::ftag
ping_pc("Request PC Approval\n<b>(FTAG Conveners)</b>"):::ftag
approve_pc("Approve\n<b>(Physics Coordinators)</b>"):::atlas
start("JIRA to 'In Progress'\n<b>(Qualifying Author)</b>"):::inst
start_date("Enter Actual Start Date in JIRA\n<b>(Subgroup Convener)</b>"):::ftag
enter("Enter in Glance\n<b>(Institute Representative</b>)"):::inst
approve_glance("Approve\n<b>(Authorship Comittee)</b>"):::atlas

chat --> create_ticket --> edit_ticket --> ping_pc --> approve_pc
approve_pc --> start --> start_date
approve_pc --> enter --> approve_glance
approve_glance --> start_date

classDef atlas fill:#c55
classDef inst fill:#3b3
classDef ftag fill:#aaa
```

Critically, the qulifying author (or their institute) should initiate <span style="font-weight:normal; background-color:#3b3">steps marked in green</span>.

In more detail:

- The interested group contacts the relevant conveners (flavor tagging conveners or any subgroup) to make sure the idea is reasonable.
- Anyone involved creates a JIRA ticket in the AFT JIRA.
    - The description doesn't need to be final.
    - The status of the ticket should be "open" to indicate that the project has not officially started.
    - Under "components" the ticket **must** list "QualificationTask"
- When the text of the QP is finalized, the relevant subgroup conveners enter the final description in JIRA.
- The flavor tagging conveners notify the physics coordinators via a JIRA comment.
- The physics coordinators approve the QP via a comment in JIRA.
- The qualifying author logs into JIRA and change the ticket status to "in progress".
- Meanwhile, the institute representative enters the text into Glance and submits the QP for approval by the authorship committee.
- After the QP is approved, the qualifying author should update the JIRA ticket with the "actual start date" as listed in Glance.
