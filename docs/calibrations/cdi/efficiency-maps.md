# Efficiency of a tagger

The efficiency of a b-jet tagger, $\epsilon_{b}$,is defined as the **probability to tag a jet correctly**, i.e.

$$
\epsilon_{b} = \frac{N_{b}^{TP}}{N_{b}} = \frac{N_{b}^{TP}}{N_{b}^{TP} + N_{b}^{FN}}
$$

where $N_{b}^{TP}$ and $N_{b}^{FN}$ are the number of true-positive and false-negative b-tags respectively. This metric also goes by other names, such as the "true-positive rate (TPR)" or the "sensitivity" of the tagger.

This definition applies to both "simple" taggers, which classify signal from background (where, in the case of a b-jet tagger, the background is composed of c- and light-jets), as well as to the case of a multi-classification taggers such as the DL1 series of heavy-flavour taggers. In the case of these multi-class taggers, you can also access c-jet and light-jet tagging information, also in the form of efficiencies, termed $\epsilon_{c}$ and $\epsilon_{l}$ respectively.

Since jet-tagging is largely centered around b-jet tagging, these c- and light-jet efficiencies are defined as the **probability to mistakenly tag a background jet as a b-jet**, i.e.

$$
\epsilon_{c/l} = \frac{N_{c/l}^{FP}}{N_{c/l}} = \frac{N_{c/l}^{FP}}{N_{c/l}^{TP} + N_{c/l}^{FN}}
$$

 which are also called the "true-negative rate (TNR)" or the "specificity", and very often you will also find the **c- or -light-jet rejection** used as a performance metric, defined as the inverse of these mis-tag efficiencies, $\frac{1}{\epsilon_{c/l}}$. 

 Put generally, the efficiency for a jet of a given flavour $f$ to be b-tagged is,

$$
\epsilon_{f} = \frac{N_{f}^{b-tagged}}{N_{f}}
$$

where $N_{f}$ is the total number of jets of a given flavour $f=b,c,l$. The corresponding rejection is therefore


$$
\epsilon_{f}^{rej.} = \frac{1}{\epsilon_{f}}
$$


### Signal vs. background

In the simplest case of a jet-tagger, wherein one wants to simply classify a jet as signal or background, then one typically forms a confusion matrix to quantify the performance of the tagger. However, in the case of tagging algorithms like the **MV2** series of BDT taggers, the output is a (1D) score in a continuous range, wherein a signal-jet would be more likely to score highly in one direction of the range, and a background-jet would (ideally) be more likely to score highly in another region of the output range. See the following example of an MV2 score distribution.

![MV2Effiency](../../assets/cdi/MV2Efficiency.png)

As you can see, the MV2 score distribution peaks at +1 for b-jets, whereas for the background it peaks at -1. It is this asymmetry that ultimately provides the discriminating power between signal and background, but it is not perfect, as you can see a large amount of overlap between the signal and background distributions. By putting a *decision boundary* (i.e. a cut) on this distribution, you can simply classify anything with a $D_{MV2} < 0.6$ as background, and signal otherwise, for example. The purity of b-jets with such a cut will be large, but there's a large number of b-jets that get "thrown out with the background", so to speak. 

Depending on the use-case, you may want to define this cut at different points of the tagger score distribution. A set of these so-called **working points** have been pre-defined by the Flavour Tagging group, labeled by their respective tagging efficiencies. The 85%, 77%, 70%, and 60% working points define cuts on the discriminant score, corresponding to *the efficiency of the decision boundary to tag a b-jet*.

### Tagging b-jets vs. c- and light-jets

The **DL1** series of DNN taggers don't simply output a single score, like the MV2 taggers, but instead they **predict the probability of the jet being a b-, c-, or light-jet**. In other words, it outputs three numbers, $p_{b}$, $p_{c}$, and $p_{u}$ for the three jet types respectively. These three numbers aren't used directly in analysis, but instead are *combined* into another (1D) discriminant score, which can be treated the same way as the MV2 tagger score above. See the following example of a DL1 score distribution, and take note of the differences in distribution shapes, and score ranges, between this tagger and the MV2 tagger above.

![DL1Effiency](../../assets/cdi/DL1Efficiency.png)


The key reason why the DL1 tagger predicts probabilities $p_{b}$, $p_{c}$, and $p_{u}$, is so that at the analysis level, we can benefit from the added flexibility to interpret the output of the tagger for c-jet tagging, and even light-jet tagging as well. This is done by changing the definition of the b-jet discriminant

$$
D_{DL1} = \log \left(\frac{p_{b}}{ f_{c}  p_{c} + (1 - f_{c})  p_{u} }\right)
$$

 to become a c- or light-jet discriminant (wherein the discriminant "target" or "signal" is typically the b-jet probability $p_{b}$ as seen above). In the end, however, the tagging efficiency is what we're interesting in measuring for the performance of these taggers.


## Measuring tagger performance

Typically, to quantify and visualize the tagger performance at a given efficiency working point (i.e. the sensitivity), we plot the efficiency against the background rejection. See below for some example plots.

![EfficiencyvsRejection](../../assets/cdi/EffvRejection.png)

You can see from this example plot that the DL1 tagger performs slightly better at lower efficiencies compared to the MV2 tagger, i.e. you get better background rejection in the higher signal purity working points. You can also note that *both* taggers have much better light-jet rejection compared to c-jet rejection, which you can visually see comes from the lower amount of overlap between the b-jet and light-jet score distributions as compared to the overlap with the c-jet distribution. 

