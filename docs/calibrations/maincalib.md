# Calibration methods

The flavour-tagging group provides calibration using data of b-, c- and light-jets.
These calibrations are performed in data using three separate analyses.

## Mainstream calibrations

These calibrations are those providing the data-to-mc scale factors included in the CDI file.

| Jet Flavour | Process | Team |
| ----------- | ------- | ---- |
| b-jets      | ttbar dilepton | Wonho Jang, Jackson Barr, Alvaro Lopez Solis, Elena Mazzeo | 
| c-jets      | ttbar l+jets | Johannes Hessler, B. Taher Saifuddin, Andrew Matha, Nikos Rompotis |
| light-jets  | Z+jets   | Leonardo Toffolin, Saad Mohiuddin, Nathalie Soybelman, Angela Burger |

