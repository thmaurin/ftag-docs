# Archived samples on tape (rel21 mainly)

Here you can find lists of archived data on tape. They were used in our rel21 calibrations and publications. We encourage everyone to archive the samples on tape.

When you need the samples again, you first need to transfer them from tape. To do so please check the instructions available in the "Info for calibrators" tab. 

## rel21 Negtag Light Jet Calibration

The samples stored in this directory have been  archived to tape:

"/eos/atlas/atlascerngroupdisk/perf-flavtag/calib/negtag/Rel21\_data15161718\_mc16ade/NewTaggers/MC16d/"

The files were split into smaller files for better handling of the datasets. The procedure is also documented in the "Info for calibrators" tab.

| Directory on EOS | Dataset Name | Archived Site |
| --- | --- | --- |
| RES\_D0\_MEAS\_0  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_res\_d0\_meas\_0 | IN2P3-CC\_GROUPTAPE | 
| RES\_D0\_MEAS\_1  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_res\_d0\_meas\_1 | IN2P3-CC\_GROUPTAPE | 
| RES\_D0\_MEAS\_2  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_res\_d0\_meas\_2 | IN2P3-CC\_GROUPTAPE | 
| RES\_D0\_MEAS\_3  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_res\_d0\_meas\_3 | IN2P3-CC\_GROUPTAPE | 
| RES\_D0\_MEAS\_4  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_res\_d0\_meas\_4 | IN2P3-CC\_GROUPTAPE | 
| RES\_D0\_MEAS\_5  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_res\_d0\_meas\_5 | IN2P3-CC\_GROUPTAPE | 
| RES\_D0\_MEAS\_6  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_res\_d0\_meas\_6 | IN2P3-CC\_GROUPTAPE | 
| RES\_D0\_MEAS\_7  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_res\_d0\_meas\_007 | IN2P3-CC\_GROUPTAPE | 
| RES\_D0\_MEAS\_8  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_res\_d0\_meas\_008 | IN2P3-CC\_GROUPTAPE | 
| RES\_D0\_MEAS\_9  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_res\_d0\_meas\_9 | IN2P3-CC\_GROUPTAPE | 
| RES\_Z0\_MEAS\_0  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_res\_z0\_meas\_0 | IN2P3-CC\_GROUPTAPE | 
| RES\_Z0\_MEAS\_1  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_res\_z0\_meas\_1 | IN2P3-CC\_GROUPTAPE | 
| RES\_Z0\_MEAS\_2  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_res\_z0\_meas\_2 | IN2P3-CC\_GROUPTAPE | 
| RES\_Z0\_MEAS\_3  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_res\_z0\_meas\_003 | IN2P3-CC\_GROUPTAPE | 
| RES\_Z0\_MEAS\_4  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_res\_z0\_meas\_4 | IN2P3-CC\_GROUPTAPE | 
| RES\_Z0\_MEAS\_5  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_res\_z0\_meas\_5 | IN2P3-CC\_GROUPTAPE | 
| RES\_Z0\_MEAS\_6  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_res\_z0\_meas\_6 | IN2P3-CC\_GROUPTAPE | 
| RES\_Z0\_MEAS\_7  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_res\_z0\_meas\_7 | IN2P3-CC\_GROUPTAPE | 
| RES\_Z0\_MEAS\_8  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_res\_z0\_meas\_8 | IN2P3-CC\_GROUPTAPE | 
| RES\_Z0\_MEAS\_9  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_res\_z0\_meas\_9 | IN2P3-CC\_GROUPTAPE |
| FAKE\_Rate\_Loose\_0  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_fake\_rate\_loose\_0 | IN2P3-CC\_GROUPTAPE | 
| FAKE\_Rate\_Loose\_1  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_fake\_rate\_loose\_1 | IN2P3-CC\_GROUPTAPE | 
| FAKE\_Rate\_Loose\_2  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_fake\_rate\_loose\_2 | IN2P3-CC\_GROUPTAPE | 
| FAKE\_Rate\_Loose\_3  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_fake\_rate\_loose\_3 | IN2P3-CC\_GROUPTAPE | 
| FAKE\_Rate\_Loose\_4  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_fake\_rate\_loose\_4 | IN2P3-CC\_GROUPTAPE | 
| FAKE\_Rate\_Loose\_5  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_fake\_rate\_loose\_5 | IN2P3-CC\_GROUPTAPE | 
| FAKE\_Rate\_Loose\_6  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_fake\_rate\_loose\_6 | IN2P3-CC\_GROUPTAPE |
| NOMINAL\_0  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_nominal\_0 | IN2P3-CC\_GROUPTAPE | 
| NOMINAL\_1  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_nominal\_1 | IN2P3-CC\_GROUPTAPE | 
| NOMINAL\_2  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_nominal\_2 | IN2P3-CC\_GROUPTAPE | 
| NOMINAL\_3  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_nominal\_003 | IN2P3-CC\_GROUPTAPE | 
| NOMINAL\_4  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_nominal\_004 | IN2P3-CC\_GROUPTAPE | 
| NOMINAL\_5  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_nominal\_005 | IN2P3-CC\_GROUPTAPE | 
| NOMINAL\_6  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_nominal\_006 | IN2P3-CC\_GROUPTAPE | 
| NOMINAL\_7  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_nominal\_007 | IN2P3-CC\_GROUPTAPE | 
| NOMINAL\_8  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_nominal\_008 | IN2P3-CC\_GROUPTAPE | 
| NOMINAL\_9  | group.perf-flavtag:ljet\_rel21\_mc16d\_trksys\_nominal\_009 | IN2P3-CC\_GROUPTAPE | 
