# Documentation of the FTAG Algorithm Group

This repository serves as documentation for the ATLAS flavour tagging group.

The docs are hosted here

- https://ftag.docs.cern.ch/

If you want to modify the documentation, please submit a merge request.

Some infrastructure that helps to populate the documentation is available at

- https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/docs-infrastructure
