# Cdi Dump

Tools which dump the CDI file content.
This code was originally built in a separate repo: https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/docs-infrastructure/cdi-dump

The code is now integrated with this repo and runs as part of the main CI.

If you want to run locally, you can follow the instructions below.
You will require access to `cvmfs`.

## Install instructions

First, set up an ATLAS analysis relase. This can be done using the `setup.sh` script.

```bash
source setup.sh
```

Then, set up a virtual environment in python such that you can install python modules.
Install the modules listed in `requirements.txt`. Both operations are combined in the script `scripts/launch_venv.sh`.

```bash
source scripts/launch_venv.sh
```

## Dump CDI content

Use the script `python/dumpCDI.py` to dump the content of all CDI files listed in `data/config.yaml`.
This may take a while, as the CDI files are read from the `/cvmfs` location specified in `data/config.yaml`.
If you do not have `cvmfs` mounted on your machine, this step will likely fail.

```bash
python python/dumpCDI.py
```

The resulting output will be located in `output/yaml/cdi_content.yaml`


Further, you can dump the efficiency maps as plots. This can be done with the script `python/plotEfficiencies.py --do_maps`.

```bash
python python/plotEfficiencies.py --do_maps
```

The resulting output will be located in `output/plots/`.

## Presenting the CDI content in markdown

Next, you can display the dumped information in a better way, using a script that parses this file and creates a markdown file with information. This is done with the script `python/createCDIOverview.py`.

```bash
python python/createCDIOverview.py
```

The resulting output will be located in `output/md/cdi_overview.md` to show an overview of the content and in `output/md/cdi_plot_overview.md` which will show all the efficiency maps.
