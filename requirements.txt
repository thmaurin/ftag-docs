mkdocs==1.5.3
mkdocs-material==9.5.1
mkdocs-git-revision-date-localized-plugin==1.2.4
mkdocs-markdownextradata-plugin==0.2.5
mkdocs-autorefs==0.5.0
mkdocs-redirects==1.2.1
pymdown-extensions==10.7.1
